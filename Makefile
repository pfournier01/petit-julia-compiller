
all: pjuliac.exe
	dune exec ./pjuliac.exe

tests: pjuliac.exe
	cd tests; ./test -3 ../_build/default/pjuliac.exe

pjuliac.exe:
	dune build pjuliac.exe

clean:
	@dune clean
	@rm tests/*/*.s
	@rm tests/a.out
	@rm test/out
	@echo "Cleaned !"

.PHONY: all clean pjuliac.exe
