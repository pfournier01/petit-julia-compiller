	
(* Petit Julia lexer *)

{
  open Lexing
  open Ast 
  open Parser
     
  exception Lexing_error of string

  let semi_colon = ref false

  let id_or_kwd =
    let h = Hashtbl.create 32 in
    List.iter (fun (s, tok) -> Hashtbl.add h s tok)
              ["if", IF; "else", ELSE; "elseif", ELSEIF;
               "return", RETURN; "end", END;
               "for", FOR;
               "function", FUNCTION;
               "mutable", MUTABLE;
               "struct", STRUCT;
               "while", WHILE;
               "true", CST (Cbool true);
               "false", CST (Cbool false)];
    fun s -> try begin
		 let x = Hashtbl.find h s in 
		 begin
                   match x with
                   | CST(Cbool(_)) | RETURN | END -> semi_colon := true
                   | _ -> semi_colon := false
                 end;
		 x
               end
             with Not_found -> IDENT s
			     
  let string_buffer = Buffer.create 1024
		    
		    
}

let letter = ['a'-'z' 'A'-'Z']
let alpha = letter | '_'
let digit = ['0'-'9']
let ident = alpha (alpha | digit )*
let integer = ['0'-'9']+
let space = ' ' | '\t'
let comment = "#" [^'\n']*
let strchar = [' '-'~'] # ['"' '\\']

rule next_tokens = parse
  | '\n'    {new_line lexbuf; if !semi_colon then (semi_colon:=false; [SEMICOLON]) else next_tokens lexbuf}
  | (space | comment)+
    { next_tokens lexbuf }
  | (integer as nb) (ident as id) {match (id_or_kwd id) with
  | IDENT s -> (semi_colon := true; [INTIDENT (int_of_string(nb),s)])
  | _ -> raise (Lexing_error ("Keyword " ^id^ " preceded by integer, illegal syntax"))}
  | (ident as id) '(' {match id_or_kwd id with
  | IDENT s -> (semi_colon := false; [IDENTLP s])
  | _ -> raise (Lexing_error ("Keyword "^id^" succeded by (, illegal syntax"))}
  | (integer as nb) '(' {[INTLP (int_of_string(nb))]}
  | ')' (ident as id) {match id_or_kwd id with
  | IDENT s -> (semi_colon := true; [RPIDENT s])
  | _ -> raise (Lexing_error ("Keyword "^id^" preceded by ), illegal syntax"))}
  | ident as id { semi_colon := true; [id_or_kwd id] }
  | '+'     { semi_colon := false; [PLUS] }
  | '-'     { semi_colon := false; [MINUS] }
  | '*'     { semi_colon := false; [TIMES] }
  | '^'     { semi_colon := false; [POW] }
  | '%'     { semi_colon := false; [MOD] }
  | '='     { semi_colon := false; [EQUAL] }
  | '!'     { semi_colon := false; [NOT] }
  | "=="    { semi_colon := false; [CMP Beq] }
  | "!="    { semi_colon := false; [CMP Bneq] }
  | "<"     { semi_colon := false; [CMP Blt] }
  | "<="    { semi_colon := false; [CMP Ble] }
  | ">"     { semi_colon := false; [CMP Bgt] }
  | ">="    { semi_colon := false; [CMP Bge] }
  | "||"    { semi_colon := false; [OR] }
  | "&&"    { semi_colon := false; [AND] }
  | "::"    { semi_colon := false; [TWOCOLON] }
  | '('     { semi_colon := false; [LP] }
  | ')'     { semi_colon := true; [RP] }
  | '.'     { semi_colon := false; [DOT] }
  | ','     { semi_colon := false; [COMMA] }
  | ':'     { semi_colon := false; [COLON] }
  | ';'     {semi_colon := false; [SEMICOLON] }
  | integer as s
    { try (semi_colon := true; [CST (Cint (int_of_string s))])
with _ -> raise (Lexing_error ("constant too large: " ^ s)) }
  | '"'     { [CST (Cstring (string lexbuf))] }
  | eof     { [EOF] }
  | _ as c  { raise (Lexing_error ("illegal character: " ^ String.make 1 c)) }


and string = parse
  | '"'
    { let s = Buffer.contents string_buffer in
Buffer.reset string_buffer; semi_colon := true; 
s }
  | "\\n"
    { Buffer.add_char string_buffer '\n';
     string lexbuf }
  | "\\\""
    { Buffer.add_char string_buffer '"';
     string lexbuf }
  | "\\\\"
    { Buffer.add_char string_buffer '\\';
      string lexbuf }
  | "\\t"
    { Buffer.add_char string_buffer '\t';
      string lexbuf }
  | strchar as c
    { Buffer.add_char string_buffer c;
      string lexbuf }
  | eof
    { raise (Lexing_error "unterminated string") }
  | _ as c
    { raise (Lexing_error ("Illegal character in string : "^ String.make 1 c))}


{

  let next_token =
    let tokens = Queue.create () in (* next tokens to be sent *)
    fun lb ->
    if Queue.is_empty tokens then begin
	let l = next_tokens lb in
	List.iter (fun t -> Queue.add t tokens) l
      end;
    Queue.pop tokens
    
  let print_token = function
    | IF -> print_string " if "
    | ELSE -> print_string " else "
    | ELSEIF -> print_string " elseif "
    | RETURN -> print_string " return "
    | END -> print_string " end "
    | FOR -> print_string " for "
    | FUNCTION -> print_string " function "
    | MUTABLE -> print_string " mutable "
    | STRUCT -> print_string " struct "
    | WHILE -> print_string " while "
    | CST(Cbool(x)) -> print_string (" const("^(string_of_bool x)^") ")
    | CST(Cint(x)) -> print_string (" const("^(string_of_int x)^") ")
    | CST(Cstring(x)) -> print_string (" const("^x^") ")
    | INTIDENT (n,s) -> print_string (" intident("^(string_of_int n)^","^s^") ")
    | IDENTLP s -> print_string (" identlp("^s^") ")
    | INTLP n -> print_string (" intlp("^(string_of_int n)^") ")
    | RPIDENT s -> print_string (" ripodent("^s^") ")
    | IDENT s -> print_string (" ident("^s^") ")
    | PLUS -> print_string " plus "
    | MINUS -> print_string " minus "
    | TIMES -> print_string " times "
    | POW -> print_string " pow "
    | MOD -> print_string  " mod "
    | EQUAL -> print_string " equal "
    | CMP Beq -> print_string " beq "
    | CMP Bneq -> print_string " bneq "
    | CMP Blt -> print_string " blt "
    | CMP Ble -> print_string " ble "
    | CMP Bgt -> print_string " bgt "
    | CMP Bge -> print_string " bge "
    | OR -> print_string " bor "
    | AND -> print_string " band "
    | NOT -> print_string " not "
    | LP -> print_string " lp "
    | RP -> print_string " rp "
    | DOT -> print_string " dot "
    | COMMA -> print_string " comma "
    | COLON -> print_string " colon "
    | SEMICOLON -> print_string " semicolon "
    | EOF -> print_string " eof "
    | TWOCOLON -> print_string " twocolon "
    | _ -> ()
}
