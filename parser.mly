
/* Petit-Julia syntactic analyser */

%{
  open Ast
  
  let rec unopt_list = function
  | [] -> []
  | Some(x)::t -> x:: (unopt_list t)
  | None :: t -> unopt_list(t)
%}

/* Token declaration */

%token IF ELSE ELSEIF RETURN END FOR
%token FUNCTION MUTABLE STRUCT WHILE
%token <Ast.constant> CST
%token <int*string> INTIDENT
%token <string> IDENTLP
%token <int> INTLP
%token <Ast.ident> RPIDENT
%token <string> IDENT
%token PLUS MINUS TIMES POW MOD EQUAL
%token NOT OR AND
%token <Ast.binop> CMP
%token LP RP DOT COMMA COLON SEMICOLON
%token TWOCOLON
%token EOF

/* Priority and predecedence rules */

%nonassoc void_return
%nonassoc RETURN
%right EQUAL
%left OR
%left AND
%left CMP
%left PLUS MINUS
%left TIMES MOD
%nonassoc unary_minus NOT
%right POW
%left DOT


/* Start of the grammar */
%start prog

/* Type of the values returned by the analyser */
%type <Ast.program> prog
%type <Ast.pos_expr> expr else_expr start_expr final_expr
%type <Ast.expr> _start_expr bloc1
%type <Ast.pos_expr> stmt_else else_bloc bloc
%type <Ast.pos_expr * Ast.pos_expr> exprbloc
%type <Ast.decl> structure func _decl
%type <Ast.pos_decl> decl
%type <Ast.type_decl> typ
%type <Ast.param> param
%type <Ast.lvalue> lvalue
%type <Ast.binop> binop


%%

/* Grammar rules */

prog:
  | l = list(decl) EOF { l }

decl: d=_decl {{d_desc = d; d_pos=$loc}}

_decl:
    | s = structure SEMICOLON { s }
    | f = func SEMICOLON { f }
    | e = expr SEMICOLON { Dexpr(e) }

structure:
    m = MUTABLE? STRUCT id = IDENT parameters = separated_nonempty_list(SEMICOLON, param?) END 
        {
            let mut =
                match m with
                | None -> false
                | Some _ -> true
            in
            Dstruct(mut,id, (unopt_list parameters))
        }

func:
    | FUNCTION id = IDENTLP parameters = separated_list(COMMA, param) RP t = typ? calc = bloc END
        {Dfunction(id, parameters, t, calc)}

typ:
    TWOCOLON t = IDENT {t}

param:
    | name = IDENT t=typ? {(Eident(name),t)}

expr : e=_expr {{e_desc= e;e_pos=$loc}}

_expr:
    | c = CST {Ecst c}
    | x = INTIDENT { Ebinop(Bmul, {e_desc=Ecst(Cint(fst x));e_pos=$loc(x)}, {e_desc=Eident(snd x);e_pos=$loc(x)}) } /* arithmetic */
    | n = INTLP b = bloc1 RP {Ebinop(Bmul, {e_desc=Ecst(Cint(n));e_pos=$loc(n)}, {e_desc=b;e_pos=$loc(b)})} /* arithmetic */
    | LP b = bloc1 RP {b}
    | LP e = expr id = RPIDENT{Ebinop(Bmul, e, {e_desc=Eident(id);e_pos=$loc(id)})}
    | id = IDENTLP e=separated_list(COMMA, expr) RP {Ecall(id, e)}
    | NOT e = expr {Eunop(Unot, e)}
    | MINUS e = expr %prec unary_minus {Eunop(Uneg, e)}
    | e1 = expr b = binop e2 = expr {Ebinop(b,e1,e2)}
    | l = lvalue {Elv(l)}
    | l = lvalue EQUAL e = expr {Eassign(l,e) }
    | RETURN %prec void_return {Ereturn({e_desc=Ecst(Cnothing);e_pos=$loc})}
    | RETURN e = expr {Ereturn e}
    | FOR id = IDENT EQUAL e = expr COLON x = exprbloc END {Efor(id, e, fst x, snd x)}
    | WHILE x= exprbloc END {Ewhile(fst x,snd x)}
    | IF x = exprbloc e2 = stmt_else {Eif(fst x, snd x, e2)}    

else_expr: e=_else_expr {{e_desc = e; e_pos=$loc}}

_else_expr:
    | c = CST {Ecst c}
    | x = INTIDENT {Ebinop(Bmul, {e_desc=Ecst(Cint(fst x));e_pos=$loc(x)}, {e_desc=Eident(snd x);e_pos=$loc(x)}) } /* arithmetic */
    | n = INTLP b = bloc1 RP {Ebinop(Bmul, {e_desc=Ecst(Cint(n));e_pos=$loc(n)}, {e_desc=b;e_pos=$loc(b)})} /* arithmetic */
    | LP b = bloc1 RP {b}
    | LP e = expr id = RPIDENT {Ebinop(Bmul,e,{e_desc=Eident(id);e_pos=$loc(id)})}
    | id = IDENTLP e = separated_list(COMMA, expr) RP {Ecall(id,e)}
    | NOT e = expr {Eunop(Unot, e)}
    | MINUS e = expr {Eunop(Uneg,e)}
    | e1 = expr b = binop e2 = expr {Ebinop(b,e1,e2)}
    | l = lvalue {Elv(l)}
    | l = lvalue EQUAL e = expr {Eassign(l,e)}
    | RETURN {Ereturn({e_desc=Ecst(Cnothing);e_pos=$loc})}
    | RETURN e = expr {Ereturn e}
    | FOR id = IDENT EQUAL e = expr COLON x = exprbloc END {Efor(id,e,fst x, snd x)}
    | WHILE x = exprbloc END {Ewhile(fst x, snd x)}

final_expr : e=_final_expr {{e_desc=e;e_pos=$loc}}

_final_expr:
    | c = CST {Ecst c}
    | x = INTIDENT {Ebinop(Bmul, {e_desc=Ecst(Cint(fst x));e_pos=$loc(x)}, {e_desc=Eident(snd x);e_pos=$loc(x)})}
    | n = INTLP b = bloc1 RP {Ebinop(Bmul, {e_desc=Ecst(Cint(n));e_pos=$loc(n)}, {e_desc=b;e_pos=$loc(b)})}
    | LP b = bloc1 RP {b}
    | LP e = expr id = RPIDENT{Ebinop(Bmul, e, {e_desc=Eident(id);e_pos=$loc(id)})}
    | id = IDENTLP e = separated_list(COMMA, expr) RP {Ecall(id,e)}
    | NOT e = final_expr {Eunop(Unot,e)}
    | MINUS e = final_expr {Eunop(Uneg,e)}
    | e1 = expr b = binop e2 = final_expr {Ebinop(b,e1,e2)}
    | l = lvalue {Elv(l)}
    | l = lvalue EQUAL e = final_expr {Eassign(l,e)}
    | RETURN e = final_expr {Ereturn(e)}
    | FOR id = IDENT EQUAL e = expr COLON x = exprbloc END {Efor(id,e,fst x, snd x)}
    | WHILE x = exprbloc END {Ewhile(fst x, snd x)}
    | IF x = exprbloc e2 = stmt_else {Eif(fst x,snd x,e2)}

start_expr : e=_start_expr {{e_desc=e;e_pos=$loc}}

_start_expr:
    | c = CST {Ecst c}
    | x = INTIDENT {Ebinop(Bmul, {e_desc=Ecst(Cint(fst x));e_pos=$loc(x)}, {e_desc=Eident(snd x);e_pos=$loc(x)}) } 
    | n = INTLP b = bloc1 RP {Ebinop(Bmul, {e_desc=Ecst(Cint(n));e_pos=$loc(n)}, {e_desc=b;e_pos=$loc(b)})} 
    | LP b = bloc1 RP {b}
    | LP e = expr id = RPIDENT{Ebinop(Bmul, e, {e_desc=Eident(id);e_pos=$loc(id)})}
    | id = IDENTLP e = separated_list(COMMA, expr) RP {Ecall(id,e)}
    | NOT e = expr {Eunop(Unot, e)}
    | e1 = start_expr b = binop e2 = expr {Ebinop(b,e1,e2)}
    | l = start_lvalue {Elv(l)}
    | l = start_lvalue EQUAL e = expr {Eassign(l,e)}
    | RETURN %prec void_return {Ereturn({e_desc=Ecst(Cnothing);e_pos=($endpos,$endpos)})}
    | RETURN e = expr {Ereturn e}
    | FOR id = IDENT EQUAL e = expr COLON x = exprbloc END {Efor(id,e,fst x, snd x)}
    | WHILE x = exprbloc END {Ewhile(fst x, snd x)}
    | IF x = exprbloc e2 = stmt_else {Eif(fst x,snd x,e2)}   
  

start_lvalue:
    | i = IDENT{LVident(i)}
    | e = start_expr DOT i = IDENT {LVfield(e,i)}
    
lvalue:
    | i = IDENT{LVident(i)}
    | e = expr DOT i = IDENT {LVfield(e,i)}


stmt_else:
    | END {{e_desc= Ecst(Cnothing); e_pos=$loc}}
    | ELSE seq = else_bloc END {{e_desc=seq.e_desc; e_pos=$loc}}
    | ELSEIF x = exprbloc e2 = stmt_else {{e_desc=Eif(fst x,snd x,e2);e_pos=$loc}}
    

%inline binop:
    | c = CMP {c}
    | PLUS {Badd}
    | MINUS {Bsub}
    | TIMES {Bmul}
    | MOD {Bmod}
    | POW  {Bpow}
    | AND  {Band}
    | OR {Bor}

else_bloc : b=_else_bloc {{e_desc=b;e_pos=$loc}}

_else_bloc:
    | e = option(else_expr)
        {
         match e with
         | None -> Eseq([])
         | Some(x) -> Eseq([x])
        }
    | e = option(else_expr) SEMICOLON l=separated_nonempty_list(SEMICOLON, expr?)
        {
         Eseq(unopt_list(e::l))
         }


exprbloc:
    | e = final_expr seq = bloc {(e,seq)}
    
bloc: b=_bloc {{e_desc=b;e_pos=$loc}}

_bloc:
    | e = start_expr?
        {
         match e with
         | None -> Eseq([])
         | Some(x) -> Eseq([x])
        }
    | e = start_expr? SEMICOLON l = separated_nonempty_list(SEMICOLON, expr?)
        {
            Eseq(unopt_list(e::l))
        }

bloc1:
    | e = expr {Eseq([e])}
    | e = expr SEMICOLON seq = bloc
        {
            match seq.e_desc with
            | Eseq(l) -> Eseq(e::l)
            | _ -> assert false
        }
