open X86_64

(** contains the assembly code the is written in the program no matter what *)

(* elementary operations *)

let type_check_int =
    (** type checking : are both operands integer-valued ? *)
    cmpq (imm 1) (ind rdi) ++
    jne "__fail" ++
    cmpq (imm 1) (ind rsi) ++
    jne "__fail"

let addition =
    (* parameters in rdi, rsi *)
    (* the parameters should be pointers to the things to add *)
    (* returns in rax : *rdi + *rsi *)
    label "__add" ++
    (* type checking *)
    type_check_int ++
    movq (ind ~ofs:8 rdi) !%rax ++ (* rax : value of the first argument *)
    addq (ind ~ofs:8 rsi) !%rax ++ (* rax : value of the sum *)
    ret

let substraction =
    (* parameters in rdi, rsi *)
    (* the parameters should be pointers to the things to substract *)
    (* returns in rax : *rdi - *rsi *)
    label "__sub" ++
    (* type checking *)
    type_check_int ++
    movq (ind ~ofs:8 rdi) !%rax ++ (* rax : value of the second argument *)
    subq (ind ~ofs:8 rsi) !%rax ++ (* rax : value of the difference *)
    ret

let multiplication =
    (* parameters in rdi, rsi
    the parameters should be pointers to the things to multiply
    returns in rax : *rdi * *rsi *)
    label "__mult" ++
    (* type checking *)
    type_check_int ++
    movq (ind ~ofs:8 rdi) !%rax ++
    imulq (ind ~ofs:8 rsi) !%rax ++
    ret

let exponentiation =

    (* parameters in rdi, rsi
    the parameters should be pointers to the true arguments
    returns in rax : *rdi^*rsi *)
    
    (* here, we implement the rapid exponentiation scheme :
    a^b = (a^2)^(b//2) if b is even,
        = a*(a^2)^(b-1//2) if b is odd *)
    label "__pow" ++
    type_check_int ++
    movq (ind ~ofs:8 rdi) !%rdi ++ (* base *)
    movq (ind ~ofs:8 rsi) !%rsi ++ (* exponent *)
    movq (imm 1) !%rax ++ (* result *)
    label "__pow_start" ++
    cmpq (imm 0) !%rsi ++ (* is %rsi nil ? *)
    je "__pow_out" ++
    testq (imm 1) !%rsi ++ (* is %rsi odd ? *)
    jne "__pow_odd" ++
    
    label "__pow_even" ++
    imulq !%rdi !%rdi ++
    shrq (imm 1) !%rsi ++
    jmp "__pow_start" ++
    
    label "__pow_odd" ++
    imulq !%rdi !%rax ++
    shrq (imm 1) !%rsi ++
    imulq !%rdi !%rdi ++
    jmp "__pow_start" ++
    
    label "__pow_out" ++
    ret

let modulo =
    (* parameters in rdi, rsi
    the parameters should be pointers to the true arguments
    returns in rax : *rdi % *rsi *)
    label "__modulo" ++
    type_check_int ++
    movq (ind ~ofs:8 rdi) !%rax ++
    movq (ind ~ofs:8 rsi) !%rsi ++
    cqto ++
    idivq !%rsi ++ (* remainder in !%rdx *)
    movq !%rdx !%rax ++
    ret
    
let division =
    (* parameters in rdi, rsi
    the parameters should be pointers to the true arguments
    returns in rax : *rdi % *rsi *)
    label "__division" ++
    type_check_int ++
    movq (ind ~ofs:8 rdi) !%rax ++
    movq (ind ~ofs:8 rsi) !%rsi ++
    cqto ++
    idivq !%rsi ++ (* quotient in !%rax *)
    ret

let cmp_beg = 
    (* tests the type compatibility of the operands with a comparison (bool or int) *)
    movq (imm 0) !%rax ++
    (* is *rdi nothing ? *)
    cmpq (imm 0) (ind rdi) ++
    je "__fail" ++
    (* is *rsi nothing ? *)
    cmpq (imm 0) (ind rsi) ++
    je "__fail" ++
    (* is *rdi an int or bool ? *)
    cmpq (imm 2) (ind rdi) ++
    jg "__fail" ++
    (* is *rsi an int or bool ? *)
    cmpq (imm 2) (ind rdi) ++
    jg "__fail"

let less =
    (* parameters in rdi, rsi
    the parameters should be pointers to the true arguments
    returns in rax : *rdi < *rsi *)
    label "__less" ++
    cmp_beg ++
    (* now, we can compare the two values *)
    movq (ind ~ofs:8 rsi) !%rax ++
    cmpq !%rax (ind ~ofs:8 rdi) ++
    setl !%al ++
    ret

let less_or_equal =
    (* parameters in rdi, rsi
    the parameters should be pointers to the true arguments
    returns in rax : *rdi <= *rsi *)
    label "__less_or_equal" ++
    cmp_beg ++
    movq (ind ~ofs:8 rsi) !%rax ++
    cmpq !%rax (ind ~ofs:8 rdi) ++
    setle !%al ++
    ret

let equal =
    (* parameters in rdi, rsi
    the parameters should be pointers to the true arguments
    returns in rax : *rdi == *rsi *)
    label "__equal" ++
    (* quick reinit *)
    movq (imm 0) !%rax ++
    
    (* check if not null pointer *)
    cmpq (imm 0) !%rdi ++
    je "__equal_end" ++
    cmpq (imm 0) !%rsi ++
    je "__equal_end" ++
    
    (* are they the same address : quick check ? *)
    cmpq !%rsi !%rdi ++
    sete !%al ++
    je "__equal_end" ++
    
    (* check the types *)
        (* are they int or bool ? *)
        cmpq (imm 2) (ind rdi) ++
        jg "__equal_struct_string" ++
        cmpq (imm 2) (ind rsi) ++
        jg "__equal_struct_string" ++
    
    movq (ind ~ofs:8 rsi) !%rax ++
    cmpq !%rax (ind ~ofs:8 rdi) ++
    sete !%al ++
    ret ++
    
        label "__equal_struct_string" ++
        (* distinguish between struct and string : they must be of the same type*)
        movq (ind rsi) !%rax ++
        cmpq !%rax (ind rdi) ++
        jne "__equal_end" ++
        cmpq (imm 3) !%rax ++ (* is it a string ? *)
        jne "__equal_struct" ++
        cmpq !%rdi !%rsi ++
        sete !%al ++
        ret ++
    label "__equal_struct" ++
    (* todo *)
    label "__equal_end" ++
    ret
    
let negation =
    (* parameter in rdi
    the parameters should be pointer to the true argument
    returns in rax : -*rdi  *)
    label "__neg" ++
    (* is it not null ? *)
    cmpq (imm 0) !%rdi ++
    je "__fail" ++
    (* is it an integer ? *)
    cmpq (imm 1) (ind rdi) ++
    jne "__fail" ++
    movq (ind ~ofs:8 rdi) !%rax ++
    negq !%rax ++
    movq !%rax (ind ~ofs:8 rdi) ++
    ret

let not_unop =
    (* parameter in rdi
    the parameters should be pointer to the true argument
    returns in rax : !*rdi  *)
    label "__not" ++
    (* is it not null ? *)
    cmpq (imm 0) !%rdi ++
    je "__fail" ++
    (* is it a bool ? *)
    cmpq (imm 2) (ind rdi) ++
    jne "__fail" ++
    movq (ind ~ofs:8 rdi) !%rax ++
    xorq (imm 1) !%rax ++
    ret

let fail =
    label "__fail" ++
    movq (ilab "__msg_error") !%rdi ++
    xorq !%rax !%rax ++
    call "printf" ++
    movq (imm 1) !%rax ++
    movq !%rbx !%rsp ++
    popq rbp ++
    ret
    
let main =
    label "main" ++
    pushq !%rbp ++
    movq !%rsp !%rbp ++ (* normal working *)
    movq !%rsp !%rbx ++ (* in case of failure : reserved register *)
    movq (imm 1) !%rdi ++
    xorq !%rax !%rax ++
    call "malloc" ++
    movq (imm 0) (ind rax) ++
    movq !%rax (indlab "_var_nothing") 
    
    

let end_main =
    popq rbp ++
    movq (imm 0) !%rax ++
    ret



let print =
    (* takes its argument on the stack, until a null pointer is encountered *)
    label "__print" ++
    cmpq (imm 0) !%rsi ++
    je "__print_end" ++
    cmpq (imm 0) (ind rsi) ++
        jne "__print_int" ++
        movq (ilab "__msg_nothing") !%rdi ++
        xorq !%rsi !%rsi ++
        xorq !%rax !%rax ++
        jmp "__print_mid" ++
    
    label "__print_int" ++
    cmpq (imm 1) (ind rsi) ++
        jne "__print_bool" ++
        (* print an integer *)
        movq (ind ~ofs:8 rsi) !%rsi ++
        movq (ilab "__msg_int") !%rdi ++
        xorq !%rax !%rax ++
        jmp "__print_mid" ++
    
    label "__print_bool" ++
    cmpq (imm 2) (ind rsi) ++
        jne "__print_str" ++
        (* print a boolean *)
        testq (imm 1) (ind ~ofs:8 rsi) ++
        jne "__print_true" ++
        movq (ilab "__msg_false") !%rdi ++
        xorq !%rax !%rax ++
        jmp "__print_mid" ++
        label "__print_true" ++
        movq (ilab "__msg_true") !%rdi ++
        xorq !%rsi !%rsi ++
        xorq !%rax !%rax ++
        jmp "__print_mid" ++
    
    label "__print_str" ++
    cmpq (imm 3) (ind rsi) ++
        jne "__print_struct" ++
        (* print a string *)
        movq (ind ~ofs:8 rsi) !%rdi ++
        xorq !%rax !%rax ++
        jmp "__print_mid" ++
    
        (* print a struct *)
        label "__print_struct" ++
        (* TODO *)
        jmp "__print_end" ++

    label "__print_mid" ++
    call "printf" ++
    ret ++
    label "__print_end"++
    ret
