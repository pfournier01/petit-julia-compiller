                                                                                                                                                     
(****************************************************************************************************************************************************
.....................................................................................................................................................
PPPPPPPPPPPPPPPPP..............................tttt............iiii..........tttt...........jjjj...................lllllll...iiii....................
P::::::::::::::::P..........................ttt:::t...........i::::i......ttt:::t..........j::::j..................l:::::l..i::::i...................
P::::::PPPPPP:::::P.........................t:::::t............iiii.......t:::::t...........jjjj...................l:::::l...iiii....................
PP:::::P     P:::::P                        t:::::t                       t:::::t                                  l:::::l                           
  P::::P     P:::::P  eeeeeeeeeeee    ttttttt:::::ttttttt    iiiiiiittttttt:::::ttttttt   jjjjjjjuuuuuu    uuuuuu   l::::l iiiiiii   aaaaaaaaaaaaa   
  P::::P     P:::::Pee::::::::::::ee  t:::::::::::::::::t    i:::::it:::::::::::::::::t   j:::::ju::::u    u::::u   l::::l i:::::i   a::::::::::::a  
  P::::PPPPPP:::::Pe::::::eeeee:::::eet:::::::::::::::::t     i::::it:::::::::::::::::t    j::::ju::::u    u::::u   l::::l  i::::i   aaaaaaaaa:::::a 
  P:::::::::::::PPe::::::e     e:::::etttttt:::::::tttttt     i::::itttttt:::::::tttttt    j::::ju::::u    u::::u   l::::l  i::::i            a::::a 
  P::::PPPPPPPPP  e:::::::eeeee::::::e      t:::::t           i::::i      t:::::t          j::::ju::::u    u::::u   l::::l  i::::i     aaaaaaa:::::a 
  P::::P          e:::::::::::::::::e       t:::::t           i::::i      t:::::t          j::::ju::::u    u::::u   l::::l  i::::i   aa::::::::::::a 
  P::::P          e::::::eeeeeeeeeee        t:::::t           i::::i      t:::::t          j::::ju::::u    u::::u   l::::l  i::::i  a::::aaaa::::::a 
  P::::P          e:::::::e                 t:::::t    tttttt i::::i      t:::::t    ttttttj::::ju:::::uuuu:::::u   l::::l  i::::i a::::a    a:::::a 
PP::::::PP        e::::::::e                t::::::tttt:::::ti::::::i     t::::::tttt:::::tj::::ju:::::::::::::::uul::::::li::::::ia::::a    a:::::a 
P::::::::P         e::::::::eeeeeeee        tt::::::::::::::ti::::::i     tt::::::::::::::tj::::j u:::::::::::::::ul::::::li::::::ia:::::aaaa::::::a 
P::::::::P          ee:::::::::::::e          tt:::::::::::tti::::::i       tt:::::::::::ttj::::j  uu::::::::uu:::ul::::::li::::::i a::::::::::aa:::a
PPPPPPPPPP            eeeeeeeeeeeeee            ttttttttttt  iiiiiiii         ttttttttttt  j::::j    uuuuuuuu  uuuulllllllliiiiiiii  aaaaaaaaaa  aaaa
                                                                                           j::::j                                                    
                                                                                 jjjj      j::::j                                                    
                                                                                j::::jj   j:::::j                                                    
                                                                                j::::::jjj::::::j                                                    
                                                                                 jj::::::::::::j                                                     
                                                                                   jjj::::::jjj                                                      
                                                                                      jjjjjj
*****************************************************************************************************************************************************)
                                                                                                                                      



open Ast

(****************************************)
(* type definitions *)


type typ =
    | Tany
    | Tnothing
    | Tint
    | Tbool
    | Tstring
    | Tstruct of Ast.ident * typ list
    | Tfunc of typ list * typ



type signature = {s_inputs : typ list;
                  s_outputs : typ}

type scope = Global | Local

type param = Ast.ident*typ

type struct_sig = {mut : bool; fields : param list}

module IdentEnv = Map.Make (struct type t = ident let compare = compare end)

module StructEnv = Map.Make (struct type t = ident let compare = compare end)

type var_env = (typ*scope) IdentEnv.t (* Map identiifier -> (type,global?) *)

type func_env = (ident, signature) Hashtbl.t
type struct_env = struct_sig StructEnv.t

type ctx = {ctx_var : var_env; ctx_fun : func_env; ctx_struct : struct_env}

type rich_lv =
    | Rlvid of ident
    | Rlvfield of rich_expr * ident

and rich_expr =
    | Rcst of constant * typ * var_env
    | Rident of ident * typ * var_env
    | Rbinop of binop * rich_expr * rich_expr * typ * var_env
    | Runop of unop * rich_expr * typ * var_env
    | Rcall of ident * (rich_expr list) * typ * var_env * scope
(*     | Rstruct of rich_expr * ident * typ * var_env *)
    | Rif of rich_expr * rich_expr * rich_expr * typ * var_env
    | Rwhile of rich_expr* rich_expr * typ * var_env * scope
    | Rfor of ident * rich_expr * rich_expr * rich_expr * typ * var_env * scope
    | Rseq of rich_expr list * typ * var_env
    | Rreturn of rich_expr * typ * var_env
    | Rassign of rich_lv * rich_expr * typ * var_env
    | Rlv of rich_lv * typ * var_env

and rich_decl =
    | RDexpr of rich_expr
    | RDstruct of bool * ident * Ast.param list
    | RDfunction of ident * Ast.param list * typ * rich_expr

(*************************************)
(* error management *)
    
exception Typing_error of string * (Lexing.position * Lexing.position)

let error s pos = raise (Typing_error(s,pos))

let rec type_to_string = function
    | Tany -> "Any"
    | Tnothing -> "Nothing"
    | Tint -> "Int64"
    | Tbool -> "Bool"
    | Tstring -> "String"
    | Tstruct(id,_) -> id
    | Tfunc(params,t) -> "Function : "^(type_list_to_string params)^" -> "^(type_to_string t)
    
and type_list_to_string = function
    | [] -> ""
    | h::t -> (type_to_string h)^" * "^(type_list_to_string t)

let type_error t1 t2 pos =
    let err_msg = "Should "^(type_to_string t1)^" and "^(type_to_string t2)^" ever be compatible, you would not be happy." in
   error err_msg pos




(*************************************)
(* helper functions *)

(* type distance *)  
let rec type_dist t1 t2 = match (t1,t2) with
    (*  t1 : type of arguments,
        t2 : type of the function's in signature *)
    | (Tany, _) -> 0
    | (_, Tany)-> 1
    | (Tnothing, Tnothing) | (Tint, Tint) | (Tbool, Tbool) | (Tstring, Tstring) -> -1
    | (Tstruct(_,l1), Tstruct(_,l2)) -> if l1 = l2 then -1 else assert false
    | (Tfunc(_,t1'), Tfunc(_,t2')) -> type_dist t1' t2'
    | _ -> assert false

let type_dist_lists = List.fold_left2 (fun n t1 t2 -> n + (type_dist t1 t2)) 0

let rec min_arg f = function
    | [] -> invalid_arg "Nothing to find the min of"
    | [x] -> [x]
    | h::t -> let r = min_arg f t in
        if f h = f (List.hd r) then h::r
        else if f h < f (List.hd r) then [h] else r

let distinct_params params = List.for_all (
    fun (x,_) -> List.length(List.filter (fun (y,_)-> x=y) params) = 1
                                          ) params
(* type lookup and edit *)
let typeof = function
    | Rcst(_,t,_) | Rident(_,t,_) | Rbinop(_,_,_,t,_)
        | Runop(_,_,t,_) | Rcall(_,_,t,_,_)
        | Rif(_,_,_,t,_) | Rwhile(_,_,t,_,_) | Rfor(_,_,_,_,t,_,_)
        | Rseq(_,t,_) | Rreturn(_,t,_) | Rassign(_,_,t,_)
        | Rlv(_,t,_) -> t

let change_type t = function
    | Rcst(x,_,e) -> Rcst(x,t,e)
    | Rident(x,_,e) -> Rident(x,t,e)
    | Rbinop(x,y,z,_,e) -> Rbinop(x,y,z,t,e)
    | Runop(x,y,_,e) -> Runop(x,y,t,e)
    | Rcall(x,y,_,e,s) -> Rcall(x,y,t,e,s)
(*     | Rstruct(x,y,_,e) -> Rstruct(x,y,t,e) *)
    | Rif(x,y,z,_,e) -> Rif(x,y,z,t,e)
    | Rwhile(x,y,_,e,s) -> Rwhile(x,y,t,e,s)
    | Rfor(i,x,y,z,_,e,s) -> Rfor(i,x,y,z,t,e,s)
    | Rseq(x,_,e) -> Rseq(x,t,e)
    | Rreturn(x,_,e) -> Rreturn(x,t,e)
    | Rassign(x,y,_,e) -> Rassign(x,y,t,e)
    | Rlv(x,_,e) -> Rlv(x,t,e)

(* type compatibility ? *)

let unopt_type = function
    | None -> Tany
    | Some(t) -> t

let typeofparam (e,may_type) = unopt_type may_type

let nameofparam (e,may_type) = match e with
    | Eident id -> id
    | _ -> ""

let compat t1 t2 = (t1 = t2) || t1 = Tany || t2 = Tany

let compat_struct t = match t with
            | Tany | Tstruct(_) -> true
            | _ -> false

let compat_field_types pos ctx id =
    (* id : name of the field *)
    (* we look up all the different structures that have this
        field *)
   let tmp = StructEnv.filter
        (fun id_s signature ->
            List.exists (fun p -> fst p = id) signature.fields
        )
        !ctx.ctx_struct in
    (* tmp : struct_sig StructEnv.t = struct_env *)
    match (StructEnv.bindings tmp) with
    | [] -> None
    | [(s_id, s_sig)] -> begin
        let r = List.filter (fun p -> fst p = id) s_sig.fields in
        match r with
        | [(id,t)] -> Some(t)
        | _ -> assert false
    end
    | _ -> error ("I still can't make divination, remove duplicate \""^id^"\".") pos

    
let check_function pos f t =
    (* we look at all the returns and verify that they are coherent with type t *)
    if t = Tany then ()
    else begin (* gather all the return types *)
        let rec gather_return_types r =
            match r with
            | Rcst(_,t,_) | Rident(_,t,_) | Rlv(_,t,_) -> ([t],false)
            | Rreturn(r_r,t,_) -> begin
                match gather_return_types r_r with
                | (x,true) -> (t::x, true)
                | (x,false) -> (x,true)
                end
            | Rseq(r_l,t,_) -> begin
                let rec aux t = function
                    | [] -> ([Tnothing],false)
                    | [x] -> gather_return_types x
                    | h::tl -> begin
                        let (x,b) = gather_return_types (Rseq(tl, t, IdentEnv.empty)) in
                        let (xh,bh) = gather_return_types h in
                        if bh then (xh,bh) else (x,b)
                    end
                in
                aux t r_l
            end
            | Runop(_,r1,t,e) (*| Rstruct(r1,_,t,e) *)| Rassign(_,r1, t,e) -> gather_return_types (Rseq([r1],t,e))
            | Rbinop(_,r1,r2, t,e) | Rwhile(r1,r2,t,e,_) -> gather_return_types (Rseq([r1;r2],t,e))
            | Rif(r1,r2,r3,t,e) | Rfor(_,r1,r2,r3,t,e,_) -> gather_return_types (Rseq([r1;r2;r3],t,e))
            | Rcall(_,r_l,t,e,_) -> gather_return_types (Rseq(r_l,t,e))
        in
        let ret_types = gather_return_types f in
        let uniq_ret_types = List.sort_uniq compare (fst ret_types) in
        match uniq_ret_types with
        | [] -> if not(compat t Tnothing) then error ("This function doesn't agree with itself. One time it returns \"Nothing\", the other it returns \""^(type_to_string t)^"\".") pos
        | [x] -> if not(compat t x) then error ("This function doesn't agree with itself. One time it returns \""^(type_to_string x)^"\", the other it returns \""^(type_to_string t)^"\".") pos
        | _ -> error "This function has several return types, you should agree with yourself." pos
    end


(* edit the context *)
let add_func_sig pos ctx id inp outp =
    try let previous_funcs = Hashtbl.find_all !ctx.ctx_fun id in
        if List.exists (fun s -> s.s_inputs = inp) previous_funcs then
            error ("Repeating yourself ? \""^id^"\" is already defined with the same signature") pos
        else
            let s = {s_inputs = inp; s_outputs=outp} in
            Hashtbl.add !ctx.ctx_fun id s
    with Not_found ->
        let s = {s_inputs = inp; s_outputs = outp} in
        Hashtbl.add !ctx.ctx_fun id s
            


let add_struct_sig pos ctx id mut params =
    let in_t = List.map snd params in
    let s = {s_inputs = in_t; s_outputs = Tstruct(id, in_t)} in
    let ss = {mut = mut; fields = params} in
    try (let _ = Hashtbl.find !ctx.ctx_fun id in
    error ("This is not good, a structure with the name of a function ? \""^id^"\" is already taken.") pos)
    with Not_found -> (Hashtbl.add !ctx.ctx_fun id s ;
                       ctx := {!ctx with ctx_struct = (StructEnv.add id ss !ctx.ctx_struct)} )

let add_var pos ctx scope id t = 
    if scope = Global && id = "nothing" then
        error "Thou shall not try to redefine nothing in a global scope" pos;
    try
        let (old_t, old_scope) = IdentEnv.find id !ctx.ctx_var in
        if scope = old_scope then begin
            if compat old_t t then
                {!ctx with ctx_var = IdentEnv.add id (old_t,scope) !ctx.ctx_var}
            else type_error old_t t pos
        end
        else
            {!ctx with ctx_var = IdentEnv.add id (t,scope) !ctx.ctx_var}
    with Not_found ->
        {!ctx with ctx_var = IdentEnv.add id (t,scope) !ctx.ctx_var}
    

(* read the context *)
let get_sig pos ctx id =
    try Hashtbl.find ctx.ctx_fun id
    with Not_found -> error ("Never heard of \""^id^"\", have you?") pos
    
(* type conversion *)

let type_conv pos ctx = function
    | "Int64" -> Tint
    | "String" -> Tstring
    | "Any" -> Tany
    | "Nothing" -> Tnothing
    | "Bool" -> Tbool
    | s -> begin
        let x = try StructEnv.find s !ctx.ctx_struct with Not_found -> error ("Trust me, I want to help you, but I can't find any structure named \""^s^"\".") pos in
        Tstruct(s, List.map snd x.fields)
    end

let param_conv pos ctx = function
    | (Eident(id), None) -> (id, Tany)
    | (Eident(id), Some(t)) -> (id, type_conv pos ctx t)
    | _ -> assert false
    
    
(* changing scope *)

let change_scope ctx e =
    (* we start by looking at all the local variables that will be defined *)
    (* then we remove them from a copy of the global scope *)
    (* and this becomes the new inital local scope *)
    (** gathering all the local variables *)
    let rec gather_local_variables e_p = match e_p.e_desc with
        | Eassign(LVident(id),_) -> [id]
        | Ecall(_,e_l) | Eseq(e_l) -> List.concat (List.map gather_local_variables e_l)
        (*| Estruct(e,_)*) | Ereturn(e) | Eunop(_,e) -> gather_local_variables e
        | Eif(e1,e2,e3) -> gather_local_variables e1 @ gather_local_variables e2 @ gather_local_variables e3
        | Ewhile(e1,e2) | Ebinop(_,e1,e2) -> gather_local_variables e1 @ gather_local_variables e2
        | Efor(x,e1,e2,e3) -> x :: (gather_local_variables e1 @ gather_local_variables e2 @ gather_local_variables e3)
        | _ -> []
    in
    let local_variables = gather_local_variables e in
    (* returning the altered context *)
    ref {!ctx with ctx_var = List.fold_left (fun env id -> IdentEnv.remove id env ) !ctx.ctx_var local_variables}


let update_ctx_scope ctx old_scope new_scope e =
    match (old_scope, new_scope) with
    | (Global, Global) -> ctx
    | (Local, Local) -> ref !ctx
    | (Global, Local) -> change_scope ctx e
    | _ -> assert false


(*****************************************)
(* enrich expression *)

let rec enrich_expr ctx (scope:scope) e_p = match e_p.e_desc with
    | Ecst(_) -> enrich_cst ctx scope e_p
    | Eident(_) -> enrich_ident ctx e_p
    | Ebinop(_,_,_) -> enrich_binop ctx scope e_p
    | Eunop(_,_) -> enrich_unop ctx scope e_p
    | Ecall(_,_) -> enrich_call ctx scope e_p
(*     | Estruct(_,_) -> enrich_struct ctx scope e_p *)
    | Eif(_,_,_) -> enrich_if ctx scope e_p
    | Ewhile(_,_) -> enrich_while ctx scope e_p
    | Efor(_,_,_,_) -> enrich_for ctx scope e_p
    | Eseq(_) -> enrich_seq ctx scope e_p
    | Ereturn(_) -> enrich_return ctx scope e_p
    | Eassign(_,_) -> enrich_assign ctx scope e_p
    | Elv(_) -> enrich_lv ctx scope e_p


and enrich_cst ctx scope e_p = match e_p.e_desc with
    | Ecst(Cnothing) -> enrich_lv ctx scope {e_p with e_desc=Elv(LVident("nothing"))}
    | Ecst(Ccar c) -> Rcst(Cstring (String.make 1 c), Tstring, !ctx.ctx_var)
    | Ecst(Cstring s) -> Rcst(Cstring s, Tstring, !ctx.ctx_var)
    | Ecst(Cbool b) -> Rcst(Cbool b, Tbool, !ctx.ctx_var)
    | Ecst(Cint n) -> Rcst(Cint n, Tint, !ctx.ctx_var)
    | _ -> assert false
    
    
and enrich_ident ctx e_p = match e_p.e_desc with
    | Eident id -> Rident(id, Tany, !ctx.ctx_var)
    | _ -> assert false


and enrich_binop ctx scope e_p = match e_p.e_desc with
    | Ebinop(bin, e1, e2) -> begin
        let r1 = enrich_expr ctx scope e1 in
        let r2 = enrich_expr ctx scope e2 in
        match bin with
        | Badd | Bsub | Bmul | Bmod | Bpow -> begin
            match (typeof r1, typeof r2) with
            | (Tint, Tint) -> Rbinop(bin, r1, r2, Tint, !ctx.ctx_var)
            | (Tint, Tany) | (Tany, Tint) | (Tany, Tany) -> Rbinop(bin, change_type Tint r1, change_type Tint r2, Tint, !ctx.ctx_var)
            | (x,y) -> type_error x y e_p.e_pos
            end
        | Band | Bor -> begin
            match (typeof r1, typeof r2) with
            | (Tbool, Tbool) -> Rbinop(bin, r1, r2, Tbool, !ctx.ctx_var)
            | (Tbool, Tany) | (Tany, Tbool) | (Tany, Tany) -> Rbinop(bin, change_type Tbool r1, change_type Tbool r2, Tbool, !ctx.ctx_var)
            | (x, y) -> type_error x y e_p.e_pos
            end
        | Beq | Bneq -> begin
            Rbinop(bin, r1, r2, Tbool, !ctx.ctx_var)
            end
        | Blt | Ble | Bgt | Bge -> begin
            let t1 = typeof r1 in
            let t2 = typeof r2 in
            begin
            match (t1, t2) with
            | (Tany, Tany) -> Rbinop(bin, r1, r2, Tbool, !ctx.ctx_var)
            | (Tint, Tany) | (Tany, Tint) | (Tany, Tbool) | (Tbool, Tany) | (Tint, Tint) | (Tint, Tbool) | (Tbool, Tint) | (Tbool, Tbool)  -> Rbinop(bin, r1, r2, Tbool, !ctx.ctx_var)
            | _ -> type_error t1 t2 e_p.e_pos
            end
        end
    end
    | _ -> assert false


and enrich_unop ctx scope e_p = match e_p.e_desc with
    | Eunop(Uneg,e) -> begin
        let r = enrich_expr ctx scope e in
        match typeof r with
        | Tint | Tbool | Tany -> Runop(Uneg, r, Tint, !ctx.ctx_var)
        | _ -> type_error (typeof r) Tint e_p.e_pos
    end
    | Eunop(Unot, e) -> begin
        let r = enrich_expr ctx scope e in
        match typeof r with
        | Tbool | Tany -> Runop(Unot, r, Tbool, !ctx.ctx_var)
        | _ -> type_error (typeof r) Tbool e_p.e_pos
    end 
    | _ -> assert false


and enrich_call ctx scope e_p = match e_p.e_desc with
    | Ecall(f_id, args) -> begin
        let r_args = List.map (enrich_expr ctx scope) args in
        if f_id = "print" || f_id = "println" then Rcall(f_id, r_args, Tnothing, !ctx.ctx_var, scope) else
        let t_args = List.map typeof r_args in
        (* get all the functions' signatures that match f_id *)
        let funcs = Hashtbl.find_all !ctx.ctx_fun f_id in
        (* keep only the compatible ones *)
        let rec compat_args tl1 tl2 = match (tl1,tl2) with
        | ([],[]) -> true
        | ([],_) | (_,[]) -> false
        | (h1::tail1, h2::tail2) -> (compat h1 h2) && (compat_args tail1 tail2)
        in
        let compat_sig = List.filter (fun s -> compat_args s.s_inputs t_args) funcs in
        (* we now keep the closest ones *)
        (* if there is none : no function matches *)
        (* if there is exactly one : a single function matches and we keep this one *)
        (* if there are several functions, there is ambiguity : error *)
        match compat_sig with
        | [] -> error "Even with all these functions, none of them is compatible. What a shame." e_p.e_pos
        | _ -> begin
            let compat_nearest = min_arg (fun s -> type_dist_lists t_args s.s_inputs) compat_sig in
            match compat_nearest with
            | [] -> assert false
            | [s] -> Rcall(f_id, r_args, s.s_outputs, !ctx.ctx_var,scope)
            | h::t -> (*failwith "ambiguity"*) Rcall(f_id, r_args, Tany, !ctx.ctx_var,scope)
        end
    end
    | _ -> assert false
        
(*and enrich_struct ctx scope e_p = match e_p.e_desc with
    | Estruct(e, id) -> begin
        let r = enrich_expr ctx scope e in
        (* checking if this can be a struct *)
        if not (compat_struct (typeof r)) then
            error "I really want to believe this is a structure, but this is *not* one" e_p.e_pos
        else
            (* getting all the possible types compatible *)
            let types = compat_field_types e_p.e_pos ctx id in
            match types with
            | None -> error ("There is no "^id^" field in any defined structure, you know, right?") e_p.e_pos
            | Some(t) -> Rstruct(r,id,t,!ctx.ctx_var)
    end
    | _ -> assert false*)

and enrich_if ctx scope e_p = match e_p.e_desc with
    | Eif(cond_e,then_e, else_e) ->
        let cond_r = enrich_expr ctx scope cond_e in
        let then_r = enrich_expr ctx scope then_e in
        let else_r = enrich_expr ctx scope else_e in
        if not (compat (typeof cond_r) Tbool) then
            type_error (typeof cond_r) Tbool cond_e.e_pos
        else begin
            let t = typeof else_r in
            if (typeof then_r) = t then
                Rif(cond_r, then_r, else_r, t, !ctx.ctx_var)
            else
                Rif(cond_r, then_r, else_r, Tany, !ctx.ctx_var)
            end
    | _ -> assert false
and enrich_while ctx scope e_p = match e_p.e_desc with
    | Ewhile(cond_e, body_e) ->
        let cond_r = enrich_expr ctx scope cond_e in
        let body_r = enrich_expr (update_ctx_scope ctx scope Local body_e) Local body_e in
        if not (compat (typeof cond_r) Tbool) then
            type_error (typeof cond_r) Tbool cond_e.e_pos
        else Rwhile(cond_r, body_r, Tnothing, !ctx.ctx_var, scope)
    | _ -> assert false
and enrich_for ctx scope e_p = match e_p.e_desc with
    | Efor(idx, begin_e, end_e, body_e) ->
        let begin_r = enrich_expr ctx scope begin_e in
        let end_r = enrich_expr ctx scope end_e in
        if not (compat (typeof begin_r) Tint) then
            type_error (typeof begin_r) Tint begin_e.e_pos
        else if not(compat (typeof end_r) Tint) then
            type_error (typeof end_r) Tint end_e.e_pos
        else begin
            let new_ctx = ref (add_var body_e.e_pos (update_ctx_scope ctx scope Local body_e) Local idx Tint) in
            let body_r = enrich_expr new_ctx Local body_e in
            Rfor(idx, begin_r, end_r, body_r, Tnothing, !new_ctx.ctx_var, scope)
        end
    | _ -> assert false
and enrich_seq ctx scope e_p = match e_p.e_desc with
    | Eseq([]) -> Rcst(Cnothing, Tnothing,!ctx.ctx_var)
    | Eseq([e]) -> enrich_expr ctx scope e
    | Eseq(h::tl) -> begin
        let rh = enrich_expr ctx scope h in
        let rt = enrich_seq ctx scope {e_desc = Eseq(tl); e_pos = (snd h.e_pos,snd e_p.e_pos)} in
        match rt with
        | Rseq(x,t,_) -> Rseq(rh::x, t, !ctx.ctx_var)
        | _ -> Rseq([rh;rt], typeof rt, !ctx.ctx_var)
    end
    | _ -> assert false
and enrich_return ctx scope e_p = match e_p.e_desc with
    | Ereturn(e) -> begin
        let r = enrich_expr ctx scope e in
        Rreturn(r,Tany, !ctx.ctx_var)
    end
    | _ -> assert false
and enrich_assign ctx scope e_p = match e_p.e_desc with
    | Eassign(lv, e) -> begin
        let r = enrich_expr ctx scope e in
        begin
            match lv with
            | LVident(id) -> begin
                ctx := add_var e_p.e_pos ctx scope id Tany;
                Rassign(Rlvid(id), r, typeof r, !ctx.ctx_var)
                end
            | LVfield(e_s, id) ->
            (* start by checking that the structure is defined *)
            let r_s = enrich_expr ctx scope e_s in
            if not (compat_struct (typeof r_s)) then
                error ("That thing left to \""^id^"\"... I'm sorry to tell you it's not a structure") e_p.e_pos
            else
                (* check if there is a field that matches and that the structure is mutable *)
                let s = StructEnv.filter (fun id_s signature -> List.exists (fun p -> fst p = id) signature.fields) !ctx.ctx_struct in
                match StructEnv.bindings s with
                | [] -> error ("Even if I would want to change this \""^id^"\" field of yours, I can't find any structure that matches it.") e_p.e_pos
                | [(s_id, s_sig)] -> begin if not s_sig.mut then
                    error ("Some things are never going to change... Code mistakes, and \""^s_id^"\" fields.") e_p.e_pos
                    else
                        let field_type = List.filter (fun x -> fst x = id) s_sig.fields in
                        match field_type with
                        | [(_,x)] -> if not (compat x (typeof r_s)) then
                                type_error x (typeof r_s) e_p.e_pos
                            else
                                Rassign(Rlvfield(r_s,id), r, typeof r, !ctx.ctx_var)
                        | _ -> assert false
                end
                | _ -> error ("Man, this is hard, trying to find a single \""^id^"\" when you gave me more than one") e_p.e_pos
                
        end
    end
    | _ -> assert false
    
and enrich_lv ctx scope e_p = match e_p.e_desc with
    | Elv(LVident(id)) -> begin 
        try Rlv(Rlvid(id), fst (IdentEnv.find id !ctx.ctx_var), !ctx.ctx_var) with
        Not_found -> error ("Who is \""^id^"\"? I don't know it. Is it behind the screen ?") e_p.e_pos
    end
    | Elv(LVfield(e_s,id)) -> begin
        (* start by checking that the structure is defined *)
        let r_s = enrich_expr ctx scope e_s in
        if not (compat_struct (typeof r_s)) then
            error ("I don't know about you, but I don't think \""^type_to_string (typeof r_s)^"\" can be a structure.") e_p.e_pos
        else
            (* can be a struct *)
            let aux s_id =
                let s_fields = try (StructEnv.find s_id !ctx.ctx_struct).fields with Not_found -> error ("Never knew of any struct with a field \""^s_id^"\", what about you?") e_s.e_pos in
                (* check if it has the right field *)
                let field_param = try List.find (fun (x,y) -> x=id) s_fields with Not_found -> error("Too bad, I didn't found a field named \""^id^"\" in the structure \""^s_id^"\".") e_s.e_pos in
                Rlv(Rlvfield(r_s,id),snd field_param, !ctx.ctx_var)
            in
            
            match r_s with
            | Rident(_,t,_) | Rcall(_,_,t,_,_) | Rif(_,_,_,t,_) | Rseq(_,t,_) | Rassign(_,_,t,_) | Rlv(_,t,_) -> begin
                match t with
                | Tstruct(s_id,_) -> aux s_id
                | Tany -> begin
                (* may be a struct, check if the field is satisfying *)
                    let satisfying = StructEnv.bindings (StructEnv.filter (fun _ s_sig -> List.exists (fun field -> (fst field) = id) s_sig.fields) !ctx.ctx_struct) in
                    (* satistfying : Ast.ident * struct_sig list *)
                    match satisfying with
                    | [] -> error ("I can't access non-existent structures. Please reconsider the choice of a field named \""^id^"\".") e_p.e_pos
                    | [(_,s_sig)] -> Rlv(Rlvfield(r_s,id),List.assoc id s_sig.fields, !ctx.ctx_var)
                    | _ -> error ("Wow, this \""^ id ^"\" field has a serious personality fragmentation, consider seeing a therapist.") e_p.e_pos
                end
                | _ -> error ("Nope, \""^(type_to_string (typeof r_s))^"\" is not a structure, sorry.") e_s.e_pos
            end
            | _ -> error ("Nope, \""^(type_to_string (typeof r_s))^"\" is not a structure, sorry.") e_s.e_pos
    end
    | _ -> assert false
    
    
(********************************)
(* global variables *)

let context = ref {ctx_var = IdentEnv.empty; ctx_fun = Hashtbl.create 64; ctx_struct = StructEnv.empty}


let res = ref []

let look_decl decl_p = match decl_p.d_desc with
    | Dstruct(b, id, params) ->
        if distinct_params params then
            begin
                add_struct_sig decl_p.d_pos context id b (List.map (param_conv decl_p.d_pos context) params)
            end
        else
            error "Want me crazy? I can't wrap my CPU around non unique fields!" decl_p.d_pos
    | Dfunction(id,params,may_type, e) ->
        let refined_params = List.map (param_conv decl_p.d_pos context) params in
        let t_f = snd (param_conv decl_p.d_pos context (Eident("[]"), may_type)) in
        add_func_sig decl_p.d_pos context id (List.map snd refined_params) t_f
    | Dexpr _ -> ()

let read_decl decl_p = match decl_p.d_desc with
    | Dstruct(b, id, params) -> res := RDstruct(b,id,params) :: !res
    | Dfunction(id, params, may_type, e) ->
        if distinct_params params then
            let refined_params = List.map (param_conv decl_p.d_pos context) params in
            let local_ctx = update_ctx_scope context Global Local e in
            let new_ctx = List.fold_left (fun ctx (id,typ) -> ref(add_var e.e_pos ctx Local id typ)) local_ctx refined_params in
            let r = enrich_expr new_ctx Local e in
            (* type of f *)
            let t_f = snd (param_conv decl_p.d_pos context (Eident("[]"), may_type)) in
            check_function e.e_pos r t_f;
            res := RDfunction(id,params, t_f, r) :: !res
        else
            error "This is troublesome... How do I know which is which when names are not injective? Perhaps you could rename some parameters?" decl_p.d_pos
      | Dexpr(e) -> res := RDexpr(enrich_expr context Global e)::!res

let read_prog (prog : pos_decl list) =
    context := {ctx_var = IdentEnv.empty; ctx_fun = Hashtbl.create 64; ctx_struct = StructEnv.empty};
    let dummy_position = Lexing.dummy_pos, Lexing.dummy_pos in
    (* creates the initial context, with predefined functions *)
    add_func_sig dummy_position context "div" [Tint; Tint] Tint;
    add_func_sig dummy_position context "print" [Tany] Tnothing;
    add_func_sig dummy_position context "println" [Tany] Tnothing;
    (* add the predefined constants *)
    context := {!context with ctx_var = IdentEnv.add "nothing" (Tnothing, Global) !context.ctx_var};
    (* add the binary/unary operators as functions for unification *)
    (* sum *)
    (**add_func_sig context "[+]" [Tint; Tint] Tint;
    (* add_func_sig context "[+]" [Tbool; Tbool] Tint;
    add_func_sig context "[+]" [Tbool; Tint] Tint;
    add_func_sig context "[+]" [Tint; Tbool] Tint; *)
    (* unary - *)
    (* add_func_sig context "[-]" [Tbool] Tint; *)
    add_func_sig context "[-]" [Tint] Tint;
    (* difference *)
    add_func_sig context "[-]" [Tint;Tint] Tint;
    (* add_func_sig context "[-]" [Tbool; Tbool] Tint;
    add_func_sig context "[-]" [Tbool; Tint] Tint;
    add_func_sig context "[-]" [Tint; Tbool] Tint; *)
    (* * *)
    add_func_sig context "[*]" [Tint; Tint] Tint;
    (* add_func_sig context "[*]" [Tint; Tbool] Tint;
    add_func_sig context "[*]" [Tbool; Tint] Tint;
    add_func_sig context "[*]" [Tbool; Tbool] Tbool;*)
    (* concatenation *)
    add_func_sig context "[*]" [Tstring; Tstring] Tstring;
    (* power *)
    add_func_sig context "[^]" [Tint; Tint] Tint;
    (* add_func_sig context "[^]" [Tint; Tbool] Tint;
    add_func_sig context "[^]" [Tbool; Tint] Tint;
    add_func_sig context "[^]" [Tbool; Tbool] Tbool; (* power, but on bools? *)*)
    (* concatenation repetition *)
    add_func_sig context "[^]" [Tstring; Tint] Tstring;
    (* add_func_sig context "[^]" [Tstring; Tbool] Tstring;*)
    (* modulo *)
    add_func_sig context "[%]" [Tint; Tint] Tint;
    (* add_func_sig context "[%]" [Tbool; Tint] Tint;
    add_func_sig context "[%]" [Tint; Tbool] Tint;
    add_func_sig context "[%]" [Tbool; Tbool] Tbool; *)
    (* bitwise or *)
    add_func_sig context "[|]" [Tbool; Tbool] Tbool;
    (* add_func_sig context "[|]" [Tbool; Tint] Tint;
    add_func_sig context "[|]" [Tint; Tbool] Tint;
    add_func_sig context "[|]" [Tint; Tint] Tint; *)
    (* bitwise and *)
    (* add_func_sig context "[&]" [Tint; Tint] Tint;
    add_func_sig context "[&]" [Tint; Tbool] Tint;
    add_func_sig context "[&]" [Tbool; Tint] Tint; *)
    add_func_sig context "[&]" [Tbool; Tbool] Tbool;**)
    List.iter look_decl prog;
    List.iter read_decl prog;
    (List.rev !res, !context)
