open Ast
open Lexing
open Parsing
open Format
open Printer
open Compiler

let usage = "usage: run [options] file.jl"

let parse_only = ref false
let type_only = ref false

let spec =
  [
    "--parse-only", Arg.Set parse_only, "  stop after parsing";
    "--typing-only", Arg.Set type_only, " stop after typing";
  ]

let file_in =
  let file = ref None in
  let set_file s =
    if not (Filename.check_suffix s ".jl") then
      raise (Arg.Bad "no .jl extension");
    file := Some s
  in
  Arg.parse spec set_file usage;
  match !file with Some f -> f | None -> Arg.usage spec usage; exit 1

let file_out = 
    (Filename.chop_suffix file_in ".jl")^".s"


let report (b,e) =
    let l = b.pos_lnum in
    let fc = b.pos_cnum - b.pos_bol + 1 in
    let lc = e.pos_cnum - b.pos_bol + 1 in
    eprintf "File \"%s\", line %d, characters %d-%d:\n" file_in l fc lc

(*
let _ =
    let inchan = open_in file in
    let lexbuf = Lexing.from_channel inchan in
    let eof = ref false in
    while not !eof do
        Lexer.print_token (
        let t  = Lexer.next_token lexbuf in
        begin match t with
            | EOF -> eof := true
            | _ -> ()
        end; t)
    done;
    close_in inchan;
    exit 0 
*)

let () =
  let c = open_in file_in in
  let lb = Lexing.from_channel c in
  try
    begin
        let f = Parser.prog Lexer.next_token lb in
        close_in c;
        (* print_decllist f;*)
        if !parse_only then exit 0
        else
            begin
(*                 Printer.print_decllist (fst(Typ0ng.read_prog f)); *)
                print_char '\n';
                let p = Typing.read_prog f in
                if !type_only then exit 0 
                else begin
                    let prog = Compiler.compile p in
                    let c = open_out file_out in
                    X86_64.print_program (Format.formatter_of_out_channel c) prog;
                    close_out c;
                    exit 0
                end
            end
    end
  with
    | Lexer.Lexing_error s -> 
	report (lexeme_start_p lb, lexeme_end_p lb);
	eprintf "lexical error: %s@." s;
	exit 1
    | Parser.Error ->
	report (lexeme_start_p lb, lexeme_end_p lb);
	eprintf "syntax error@.";
	exit 1
	| Typing.Typing_error(s,pos) ->
	report pos;
	eprintf "typing error: %s@." s;
    | e ->
    report (lexeme_start_p lb, lexeme_end_p lb);
	eprintf "Anomaly: %s\n@." (Printexc.to_string e);
  exit 2
