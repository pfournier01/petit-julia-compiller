open Ast
open Lexing
open Parsing
open Format
open Typing



let print_unop = function
   |Unot -> print_string "NOT "
   |Uneg -> print_string " -"

let print_binop = function
  | Badd -> print_string "+"
  | Bsub  -> print_string "-"
  | Bmul  -> print_string "*"
  | Bmod  -> print_string "%"
  | Bpow   -> print_string "^"  (* + - * % ^ *)
  | Beq  -> print_string "=="
  | Bneq  -> print_string "!="
  | Blt  -> print_string "<"
  | Ble  -> print_string "<="
  | Bgt  -> print_string ">"
  | Bge   -> print_string ">="(* == != < <= > >= *)
  | Band  -> print_string "AND"
  | Bor -> print_string "OR"

let print_cst = function
    | Cnothing -> ()
    | Ccar ch -> print_char ch
    | Cstring st -> print_string st
    | Cbool b -> print_bool b
    | Cint i -> print_int i 


let rec print_expr expr = 
  match expr with
  | Ecst constant -> print_cst constant
  | Eident  ident -> print_string ident
  | Ebinop  (binop, e1, e2)-> begin
    print_string "binop( ("; 
    print_expr e1.e_desc; print_string ") "; 
    print_binop binop; print_string " (";
    print_expr e2.e_desc; print_string ") )" end 
  | Eunop (u,expr) -> (print_unop u ; print_expr expr.e_desc; print_string ")")
  | Ecall (ident, expr_list) -> (print_string "Call ("; print_string ident; print_exprlist expr_list; print_string ") ")
(*   | Estruct (e, ident) -> (print_string ident; print_string "("; print_expr e.e_desc; print_string ")") *)
  | Eif (e1,e2,e3) -> (
    print_string " IF "; 
    print_expr e1.e_desc; 
    print_string " THEN "; 
    print_expr e2.e_desc; 
    print_string " ELSE "; 
    print_expr e3.e_desc)
  | Ewhile (e1,e2) -> (
    print_string "WHILE "; 
    print_string "("; 
    print_expr e1.e_desc; 
    print_string ")"; 
    print_string " DO "; 
    print_string "("; 
    print_expr e2.e_desc;
    print_string ")")
  | Efor (id, e1, e2, e3) -> (print_string "FOR "; 
    print_string "id"; 
    print_string " from "; (* var, start, end, body *)
    print_expr e1.e_desc;
    print_string " to ";
    print_expr e2.e_desc;
    print_string " DO ";
    print_expr e3.e_desc)
  | Eseq l -> print_exprlist l
  | Ereturn e -> (print_string "RETURN ("; print_expr e.e_desc; print_string ")")
  | Eassign (lv, e) -> (
    print_lvalue lv; 
    print_string " <- "; 
    print_expr e.e_desc) 
  | Elv lv -> print_lvalue lv

and print_exprlist = function
    |t::q ->
    (print_string " ("; print_expr t.e_desc; print_string ")\n" ;print_exprlist q)
    | [] -> ()
and print_lvalue = function
    | LVident id -> print_string id
    | LVfield (e, id) -> (print_string "("; print_expr e.e_desc; print_string "."; print_string id; print_string ")" )
  
let rec print_paramlist list = 
  match list with 
    | t::q -> (print_expr (fst(t)); print_string " " ; print_paramlist q)
    | [] -> ()



let rec print_decl = function
    | RDexpr r -> print_rich_expr r
    | RDstruct (b,i,p) -> begin 
      print_string ("struct("); 
      print_bool (b); print_string " "; 
      print_paramlist (p); 
      print_string(")") end
    | RDfunction (i,p,t,r) -> begin 
    print_string "function("; print_string(i^"[");print_typ t; print_string " :"; print_paramlist (p); print_rich_expr r; print_string ")" end

and print_decllist = function
    | tete::queue -> begin print_decl tete; print_string "\n"; print_decllist queue end
    | [] -> ()

and print_typ = function
  | Tany -> print_string "'a"
  | Tnothing -> print_string "nothing"
  | Tint-> print_string "int"
  | Tbool-> print_string "bool"
  | Tstring-> print_string "string"
  | Tstruct(_,list) -> (print_string " struct(" ;print_typ_list list; print_string")")
  | Tfunc (list, t) -> (print_string " function("; print_typ_list list ; print_string " -> "; print_typ t; print_string")")

and print_typ_list = function
  | [] -> ()
  | t::q -> (print_typ t; print_string " "; print_typ_list q)

and print_type typ =
    (print_string "["; print_typ typ; print_string "]")
and pt typ = ()

and print_rlv = function
    | Rlvid(id) -> print_string id
    | Rlvfield(r,id) -> print_string "( "; print_rich_expr r; print_string "."; print_string id ; print_string " )"

and print_rich_expr expr =
  match expr with
  | Rcst (constant, t, _) -> (print_cst constant; pt t)
  | Rident  (ident, t, _) -> (print_string ident; pt t)
  | Rbinop  (binop, e1, e2, t, _)-> begin
    print_string "binop( ("; 
    print_rich_expr e1; print_string ") "; 
    print_binop binop; print_string " (";
    print_rich_expr e2; print_string ") )"; 
    pt t end 
  | Runop (u,expr,t, _) -> (print_unop u ; print_rich_expr expr; print_string ")"; pt t)
  | Rcall (ident, expr_list, t, _,_) -> (print_string "Call ("; print_string ident; print_rich_exprlist expr_list; print_string ")"; pt t)
  (*| Rstruct (e, ident,t, _) -> (print_string ident; print_string "("; print_rich_expr e; print_string ")"; pt t)*)
  | Rif (e1,e2,e3,t, _) -> (
    print_string " IF "; 
    print_rich_expr e1; 
    print_string "\n\tTHEN "; 
    print_rich_expr e2; 
    print_string "\n\tELSE "; 
    print_rich_expr e3; pt t)
  | Rwhile (e1,e2,t, _,_) -> (
    print_string "WHILE "; 
    print_string "("; 
    print_rich_expr e1; 
    print_string ")"; 
    print_string " DO\n"; 
    print_rich_expr e2;
    print_string "END_WHILE"; pt t;print_newline ())
  | Rfor (id, e1, e2, e3, t, _,_) -> (print_string "FOR "; 
    print_string "id"; 
    print_string " from "; (* var, start, end, body *)
    print_rich_expr e1;
    print_string " to ";
    print_rich_expr e2;
    print_string " DO\n";
    print_rich_expr e3; pt t;
    print_string " END_FOR\n")
  | Rseq (l,t, _) -> (print_rich_exprlist l; pt t)
  | Rreturn (e,t, _) -> (print_string "RETURN ("; print_rich_expr e; print_string ")"; pt t)
  | Rassign (lv, e,t, _) -> (
    print_rlv lv; 
    print_string " <- "; 
    print_rich_expr e; pt t;) 
  | Rlv (lv,t, _) -> (print_rlv lv; pt t)
and print_rich_exprlist list = 
  match list with 
    |t::q ->
    (print_string " ("; print_rich_expr t; print_string ")\n" ; print_rich_exprlist q)
    | [] -> ()
