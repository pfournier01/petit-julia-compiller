(* Petit-Julia abstract syntax tree *)

type ident = string (* identificators *)

type unop = (* unary operators *)
  | Uneg (* -e *)
  | Unot (* !e *)

type binop =  (* binary operators *)
  | Badd | Bsub | Bmul | Bmod | Bpow    (* + - * % ^ *)
  | Beq | Bneq | Blt | Ble | Bgt | Bge  (* == != < <= > >= *)
  | Band | Bor

type constant =
  | Cnothing
  | Ccar of char
  | Cstring of string
  | Cbool of bool
  | Cint of int
  
let eq c1 c2 = match(c1, c2) with
    | (Cnothing,Cnothing) -> true
    | (Cnothing, _) | (_, Cnothing) -> false
    | (Ccar(c), Cstring(s)) | (Cstring(s),Ccar(c)) -> (String.make 1 c) = s
    | (Ccar c1, Ccar c2) -> c1 = c2
    | (Cstring s1, Cstring s2) -> s1 = s2
    | (Cbool b1, Cbool b2) -> b1 = b2
    | (Cint n1, Cint n2) -> n1 = n2
    | (Cbool b, Cint n) | (Cint n, Cbool b) -> (n<>0) = b
    | _ -> false

type expr = (* expressions *)
    | Ecst of constant
    | Eident of ident
    | Ebinop of binop * pos_expr * pos_expr
    | Eunop of unop * pos_expr
    | Ecall of ident * pos_expr list
(*     | Estruct of pos_expr * ident (*e.ident dans le cadre d'une structure*) *)
    | Eif of pos_expr * pos_expr * pos_expr
    | Ewhile of pos_expr * pos_expr
    | Efor of ident * pos_expr * pos_expr * pos_expr (* var, start, end, body *)
    | Eseq of pos_expr list
    | Ereturn of pos_expr
    | Eassign of lvalue * pos_expr
    | Elv of lvalue

and pos_expr = {e_desc : expr; e_pos : Lexing.position * Lexing.position}
and type_decl = string

and decl  =
    | Dexpr of pos_expr
    | Dstruct of bool * ident * param list  (* mutable?, name, fields *)
    | Dfunction of ident * param list * type_decl option * pos_expr (* name, arguments, result *)
and pos_decl = {d_desc : decl; d_pos : Lexing.position * Lexing.position}

and param =  expr * type_decl option (* ident * type *)
and lvalue = LVident of ident | LVfield of pos_expr * ident
and program = pos_decl list
(*
let rec simplify = function
| Ecst(Ccar c) -> Ecst(Cstring (String.make 1 c))
| Eunop(Unot,Ecst(Cbool(b))) -> Ecst(Cbool(not b))
| Eunop(Uneg,Ecst(Cint(n))) -> Ecst(Cint(-n))
| Eseq([]) -> Ecst(Cnothing)
| Eseq([x]) -> simplify x
| Eseq(h::t) ->
begin
    let x = simplify (Eseq(t)) in
    let h' = simplify h in
    match x with
    | Eseq(t') -> Eseq(h'::t')
    | _ -> simplify (Eseq(h'::[x]))
end
| Ebinop(Beq,Ecst(c1),Ecst(c2)) -> Ecst(Cbool(c1=c2))
| Ebinop(Bneq, Ecst(c1),Ecst(c2)) -> Ecst(Cbool(c1<>c2))
| Ebinop(Blt, Ecst(Cint(n1)),Ecst(Cint(n2))) -> Ecst(Cbool(n1<n2))
| Ebinop(Ble, Ecst(Cint(n1)),Ecst(Cint(n2))) -> Ecst(Cbool(n1<=n2))
| Ebinop(Bgt, x,y) -> simplify (Ebinop(Blt, y,x))
| Ebinop(Bge, x,y) -> simplify (Ebinop(Ble, y,x))
| Ebinop(Band, Ecst(Cbool(b1)),Ecst(Cbool(b2))) -> Ecst(Cbool(b1 && b2))
| Ebinop(Bor, Ecst(Cbool(b1)),Ecst(Cbool(b2))) -> Ecst(Cbool(b1 || b2))
| Ebinop(b, e1, e2) as e ->
    let x = Ebinop(b, simplify e1, simplify e2) in
    if x <> e then
        simplify x
    else x
| Eunop(u,e) as e' ->
    let x = Eunop(u, simplify e) in
    if x <> e' then
        simplify x
    else x
| Ecall(id, e) -> Ecall(id, List.map simplify e)
| Estruct(e,id) -> Estruct(simplify e, id)
| Eif(e1,e2,e3) -> Eif(simplify e1, simplify e2, simplify e3)
| Ewhile(e1, e2) -> Ewhile(simplify e1, simplify e2)
| Efor(id, e1, e2, e3) -> Efor(id, simplify e1, simplify e2, simplify e3)
| Ereturn(e) -> Ereturn(simplify e)
| Eassign(lv, e) -> Eassign(lv, simplify e)
| _ as x -> x
*)
