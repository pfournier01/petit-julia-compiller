# Petit Julia Compiler

## Synopsis

* This project is now an (almost) fully functional compiler for Petit-Julia. It takes a `.jl` file as input, then processes it to check if everything is valid. It is capable of producing an abstract syntax tree decorated with types, but as this is not used anywhere now, it is just thrown away. When this analysis phase is done, it creates assembly code (following the AT&T x86_64 syntax).
* If the input code is incorrect, it should raise an error, of the corresponding type (lexical, syntax, typing, compilation, running-time).

## Table of contents

> * [Petit Julia Syntax Analyser](#Petit-Julia-Syntax-Analyser)
>   * [Synopsis](#Synopsis)
>   * [Table of contents](#table-of-contents)
>   * [Usage](#usage)
>   * [Code](#code)
>       * [`lexer.mll`](#\`lexer.mll\`)
>       * [`ast.ml`](#\`ast.ml\`)
>       * [`parser.mly`](#\`parser.mly\`)
>       * [`typing.ml`](#\`typing.ml\`)
>       * [`printer.ml`](#\`printer.ml\`)
>       * [`compiler_aux.ml`](#\`compiler_aux.ml\`)
>       * [`compiler.ml`](#\`compiler.ml\`)
>       * [`pjuliac.ml`](#\`pjuliac.ml\`)
>   * [Limitations](#limitations)
>   * [Difficulties encountered](#Difficulties-encountered)
>   * [Extra features](Extra-features)


## Usage

Before getting into the code, a brief mention on how to use it.

Here is provided the source code for the compiler, written in  `OCaml`. It is compiled using the tool `make`, which uses internally `dune`.

- Creating the compiler :

  When in the folder, simply run `make pjuliac.exe`. It should compile without any hurdle. Congratulations, you now have your own compiled version of the unofficial compiler from frustrated developers for `Petit-Julia`.

  Note : it requires `OCaml` version 4.10.0 or more. It was internally tested with `OCaml` version 4.11.1.

- Testing the compiler :

  In case the user wants to check how ~~poorly~~ well this compiler works, they can download the `tests` folder designed by Jean-Christophe Filliatre, put it in the `pjuliac` folder, and run `make test`. Only one test should fail when it should run smoothly : `tests/exec/int64.jl`, for reasons detailed in the [Limitations](#limitations) section.

- Getting rid of the compiler :

  In case you wanted to remove our compiler, in addition to making our team sad,  you can simply run a `make clean` to get rid of all the compiled files. For further annihilation, remove the folder altogether.

- Using manually the compiler :

  When the `make` command is done, a link `pjuliac.exe` is created in the current folder. You can then use `./pjuliac.exe` to run the compiler. The syntax is the following : `./pjuliac.exe [options] file.jl`. It will run the compiler on the file `file.jl` with the options furnished. The options are the following :

  - `--parse-only` : stops after the parsing
  - `--typing-only` : stops after the typing
  - `--help` or `-help` : displays this list of options.

## Code


### `lexer.mll`

* This file is the description of a lexer that will be read by `ocamllex` to create an automaton capable of transforming a stream of characters into a stream of tokens (elementary units of sense in the code), while also rejecting syntax errors.
* It is also responsible for keeping a track of the position of the tokens in the file, so the errors can point to the right place in the program, at all stages of the compilation.
* It also has a function for printing the tokens in `stdout`.
* Overall, this is pretty much just translation of the syntax of Petit-Julia, nothing particularly clever here.

### `ast.ml`

* The abstract syntax tree of Petit-Julia. It provides the rest of the project with the types required to navigate and interact with the syntax of Petit-Julia. These types are such that every node of the AST are decorated with its location in the file.
* It used to have a function that simplified the AST as well : whenever a computation arose where the result could easily be computed in advance, it would simplify it. For example `3 == 2` would be simplified to `false`. It simplified all the comparisons and unary operations. This no longer works since it relied on an older type of AST where the positions of the rules were not indicated. It would not be too hard to make it work again, but, time being a major constraint, we have decided not to maintain it for now.

### `parser.mly`

* The descriptor for `menhir` of the grammar of Petit-Julia. It converts a stream of tokens into an AST. It is significantly beefier than the grammar provided by the subject for two main reasons :
  * Remove ambiguity and conflicts for `menhir`. The provided grammar, while understandable by the fellow humans designing this project, was not detailed enough for `menhir`. That's why there are several variants of `expr` (including `start_expr`, `final_expr`...), and overall more distinctions in the cases.
  * Have an easier time giving the result decorated with the position. To avoid repetitive writing, in order both to keep our sanity, and to have a code that is easier to debug, we have put, as much as possible, the decoration of the AST in a separate grammar rule (`expr` and `_expr` for instance).
* This was a difficult part, because the debugging was tedious : trying to understand the documentation of `menhir`, trying to understand why `menhir` was complaining, and trying to find a way to fix it while not breaking everything else around it.

### `typing.ml`

* Perhaps the longest part of the project so far. What this part does is getting the AST, and twists it in order to squeeze every tiny type/scope/definition error it can, while also producing a typed AST in the end.
* It can detect a whole lotta ~~love~~ errors : a function that is not defined when used, a function called but no defined function matches the arguments, the call of a field on a structure that is not existent, scope errors, and so on.
* Caution is advised : the typer tends to be passive agressive when reporting errors (which may or may not reflect the dev's feeling about Julia typing).
* If everything went well, it produces, without complaining, an AST with all the types inferred statically.
* We tried to implement a way to notice when the dispatch would fail (when there's an ambiguity that is critical), but it was way too violent, so we commented it out. It relied on computing the type distance between the argument of a function and the input types for all the variants of the callee.
* We also envisioned to make all the binary operators into function calls, as it would be the case in `OCaml` for instance. We have not yet decided to keep it, this is a decision we will take during the code production phase. It is semi-implemented and commented out for now.
* What's more, the file should be relatively well organised despite its lengths. You will find, in this order :
  * SDAA
  * Type definitions
  * Error management functions
  * Helper functions
  * Type compatibility checking functions
  * Context editing utilities
  * Type conversion
  * Scope update
  * Expression enrichment
  * Global scope creation and reading of the file

### `printer.ml`

* The Not-So-Pretty Printer^TM^ (NSPP for short) is contained in `printer.ml`. While not really pretty, either in its source code or production, it is useful to print debugging information, such as dumping the content of the AST in a format decipherable for a human.

### `compiler_aux.ml`

- Contains the primitives of some operations.
  - All binary operators (`+`,`-`,`*`,`%`,`^`,`==`,`!=`,`<=`,`<`,`>`,`>=`,`&&`,`||`)
  - All unary operators (`!`,`-`)
  - The `print` directive
  - The `div` implementation
- Basically a data file containing all the assembly code that is necessary for all and every Petit-Julia code, a kind of standard library, in a way.

### `compiler.ml`

- Other than some auxiliary helper functions, it contains most of the code production logic

- The code productions follows this sequence :

  - Compile the header of the assembly file (most of the directives defined in `compiler_aux.ml`)
  - Compile what will end the code part of the assembly part (end of the `main`, `__fail` directive)
  - Compile the declarations of structure and functions
  - Compile the dynamic dispatch of the functions
  - Compile the sequence of expressions that follow
  - Compile the `.data` segment (strings (including the always defined format strings), global variables)
  - Put everything together in a single block code, and output it

- Some details are needed on the choices made during code production :

  - Every value is represented in the same way : it is a pointer to a memory block, containing the type and the data. For instance, if `rax` contains the integer `42`, we will have `(rax) == 1` (for the type) and `8(rax) == 42` (the value). An exception is made for strings : the "data" is a pointer to the string defined in the data segment.

    |                         Value                         |                    Memory representation                     |
    | :---------------------------------------------------: | :----------------------------------------------------------: |
    |                       `nothing`                       |                        `NULL` pointer                        |
    |                       `n:Int64`                       |                           `[1;n]`                            |
    |                       `b:Bool`                        |  `[2;b]` (with `b` = 1 or 0 if it is true or false (resp.))  |
    |                      `s:String`                       | `[3;*s]` (with `*s` a pointer to the address containing the string `s`) |
    | `s:S` with `S` a structure with fields `x1;x2;...;xn` | `[t;*s.x1;*s.x2;...;*s.xn]` (where `t>4` is a numeric identifier unique to `S`, and `*s.xi` is the pointer to the `i`th field of `s`) |

  - Even constants are put on the heap every time they are encountered. This is convenient, because easy and systematic to implement, however, this leads to severe memory leaks as they are effectively dangling after use (for instance `while true 1 end` only runs ~1 millions loops when tested, before stopping when running out of memory). This could be solved by keeping track, at compilation time, of all the constants and assign a global variable to them for instance (like it is done with strings).

  - Memory allocation is done through `malloc`. Most of the time, memory is not freed.

  - Global variables are instantiated in the `.data` segment, as a pointer.

  - Local variables are on the stack. The offset to the initial value of `rsp` is stored at compile time. The initial value of `rsp` is stored in a register at execution time.

  - Unary and binary operators are a call to the function predefined in `compiler_aux.ml`. The arguments are put in `rdi` and `rsi` as per calling convention, and returned in `rax`. As usual, it is always pointers that are passed.

    - Most of these are just type checking and calling the good assembly operation, and then returning.
    - Exponentiation is defined manually, through a rapid exponentiation scheme (runs in time logarithmic in the exponent).

  - If then else conditionals are just evaluating the condition, checking that it is a boolean, and depending on its value, jump to one label or the other.

    - Labels are guaranteed to be unique thanks to a counter that is incremented for each ITE encountered in the text.

  - While loops are identified thanks to their unique identifier number, akin to the ITE constructions.

    - The local scope is implemented, and all the local variables that are created and allocated are then freed.

  - For loops (to be fixed) are again identified thanks to their unique identifier number.

    - The counter and upper bound are to be stored independently of the local variable representing the index, in order to correctly compile loops like `for x=0:10 x=11 print(x) end`, that are supposed to print 11 times 11, and not just once.

  - When assigning a variable (or a field of a structure), we check if it local, or global, and then if it has already been defined or not to either create a new memory block, or modify the previous one.

  - When structures are defined, they also come bundled with their constructor, which is just a regular function, submitted to the dispatch, that creates the right memory representation of the structure.

  - When functions are created, they are assigned a prefix, of the form `_f_t1_..._tn_r_to_` where `t1,...,tn` are the types of the arguments and `to` is the type of the output. This way, each one of the different functions sharing a same name will be differentiated. It has also been designed such that it can not make any label collision. The function's arguments are to be passed as pointers, on the stack. It returns a pointer to the answer in `rax`.

  - Dispatch is done in a brute-force way. We collect all the functions that share the same name and number of parameters, then partition them by "argument precision" (i.e. the number of "Any" in the types of the arguments), then we methodically check, for every bucket, all the functions, remember, when one has been found, its address, and fail if two correct functions are found.

    - By treating them by increasing number of `Any`, we are sure not to raise an ambiguity when there is not, and to find the most specific function to be called.

  - Function calls compute all the arguments, check their type, puts them on the stack, and then calls the dispatch procedure.

### `pjuliac.ml`

* The conductor. Its conception is rather minimal, as all it does it chit-chat with the user.
* It reads the arguments given in the command line, gives all the menial jobs to the aforementioned modules, and then tells the user whether or not something went horribly wrong, and if everything went well, produces the assembly code.

## Limitations

* The integers close to `Int.max_int` and `Int.min_int` can be a little buggy (namely $-2^{63}$ is not accepted while it should be). This is something we plan on fixing in the future, either by adding a specific rule for it, or by changing the way integers are represented in memory during compilation.
* The typing is a little more strict than planned: `return1` and `undef1` and `undef3-5` fail the typing and therefore do not compile (they are planned to fail at execution time). 
* The compiler is unable to handle 64 bit integers like Julia is supposed to, instead being limited to 32 bits (therefore failing some arithmetic tests, this could probably be fixed by managing them in two times, or using specific instructions).
* Dispatch still encounters a few issues we didn't have the time to fix (worked fine when testing locally on simple examples). 
* Questionable management of local scopes can sometimes lead to errors in recursive functions. This is due to stack alignment issues most of the time, and errors that accumulate when going in and out of scopes repeatedly.
* Structures are not fully implemented yet : they can not be printed nor compared directly. We planned on doing the equality test physically.

## Difficulties encountered

- The typer was, overall, quite subtle and difficult, mainly because of the way Julia manages the scopes, and the fact that it is dynamically typed.
- Managing the stack was sometimes bothersome (having to be careful to always clean everything). In fact, that's the source of almost all the bugs we encountered and did not fix on time (leaving things on the stack, return address not on top of the stack...). This is also due to scopes being rather painful to manage. I suspect that by taking a few days more to properly analyze and fix all the `SEGFAULT` that appear, this would have fixed most of the other bugs present here.
- Function dispatch was probably the hardest thing to design, yet not the worse to debug.

## Extra features

* Super Dope ASCII Art (SDAA for short), progressively becoming more tortured as time and our own dissatisfaction regarding Julia went on.
* Added support for indirect labels `(_my_label_)` in `x86_64.ml`. This is a x86 feature that was not present in the library, and implementing it avoids having to compute the address first.