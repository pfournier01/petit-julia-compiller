open Typing
open X86_64
open Compiler_aux

(* memory representation of the values : each value is a pointer (ptr) to a stack address
    in the stack, one word represents the class
    the following words represent the value of the object
*)

type ptr = int
(*type value = 
    | Vint of int (* class = 1 *)
    | Vbool of int (* class = 2 *)
    | Vstring of ptr (* class = 3 *)
    | Vstruct of ptr array  (* class >= 4 *)
    | Vnothing (* class = 0 *)
    | Vundefinded*)

module IdMap = Map.Make(String) (* local variable -> %rbp offset in bytes *)
let (genv : (Ast.ident, unit) Hashtbl.t) = Hashtbl.create 32 (* global variables *)
let _ = Hashtbl.add genv "nothing" ()
let (idxenv : (Ast.ident, unit) Hashtbl.t) = Hashtbl.create 32 (* indexes *)
let loc_var = ref IdMap.empty (* getting access of the local variables *)
let structs = ref IdMap.empty (* name of the structure -> number representing the type *)
let name_of_struct = ref IdMap.empty (* field name -> struct name *)
let num_of_field = ref IdMap.empty (* field name -> offset *)
let next_struct = ref 4

(* prefix of the form : _f_t1_t2_..._tn_r_t_ with ti the numeric identifier of the type of the parameter, being A if Any, and t be the type of return *)
let (func_sig : (Ast.ident, string) Hashtbl.t) = Hashtbl.create 32

let next = ref 0 (* offset of the next local variable to be defined *)
let (strings : (string, int) Hashtbl.t) = Hashtbl.create 32 (* strings *)
let string_counter = ref 0 (* number of strings defined *)
let ite_counter = ref 0 (* number of if/then/esle encountered *)
let while_counter = ref 0 (* number of while loops encountered *)
let for_counter = ref 0 (* number of for loops encountered *)
let skip_counter = ref 0
let or_counter = ref 0
let and_counter = ref 0


let decl = ref nop
let expr = ref nop

let rec max_local_vars (r:Typing.rich_expr) =
    match r with
    | Rcst _ -> 0
    | Rident(id, _, e) | Rlv((Rlvid id), _, e) -> 0
    | Rbinop(_ ,r1, r2, _, _)  | Rwhile(r1, r2, _, _,_) -> (max_local_vars r1) + (max_local_vars r2)
    | Runop(_, r1, _, _) | Rreturn(r1, _, _) -> max_local_vars r1
    | Rassign(Rlvid(id), r1, t, e) -> max_local_vars r1 + (try let (_,s) = IdentEnv.find id e in if s = Local then 1 else 0 with Not_found -> 0)
    | Rassign(Rlvfield(r1,id), r2, t, e) -> max_local_vars r1 + max_local_vars r2
    | Rcall(_, rl, _, _,_) | Rseq(rl, _, _) -> List.fold_left (fun n r -> n + max_local_vars r) 0 rl
    | Rif(r1, r2, r3, _, _) -> (max_local_vars r1) + (max_local_vars r2) + (max_local_vars r3)
    | Rfor(id, r1, r2, r3, _, e,_) -> (max_local_vars (Rident(id, Typing.Tint, e))) + (max_local_vars r1) + (max_local_vars r2) + (max_local_vars r3) + 2
    | Rlv((Rlvfield(r1, id)), _, e) -> max_local_vars r1

(* things to do :
    structure equality
*)

(*
    Labels to implement :
    - if then else branches
    - for and while loops
    - functions
*)

(*
    Suggestion :
    - different failure types (null pointer exn, type error...)
*)

(* change of paradigm : all standard operations will be function calls
    this way, the code will be cleaner : in the header, all the standard functions will be defined, and we can just call them whenever need be.
    These functions can also have their own reserved labels for conditionnal output
*)

let get_type reg offs =
    (** puts the type of the value in offs(rbp) in !%reg *)
    movq (ind ~ofs:(-offs) rbp) !%reg ++
    movq (ind reg) !%reg

let check_types offs clas =
    (** checks that offs(rbp) has the type clas *)
    (* we need to get the first word of the value pointed by offs, and compare it with clas *)
    (* offs >= 0, in bytes *)
    movq (ind ~ofs:(-offs) rbp) !%rax ++ (* rax contains the memory address of the value we're checking *)
    cmpq  (ind rax) (imm clas) ++ (* (rax) contains the class *)
    jne "_fail"

(** memory allocation **)

let allocate size =
    (** allocates size bytes of data on the heap *)
    pushq !%rdi ++
    movq (imm (8*size)) !%rdi ++ 
    call "malloc" ++
    popq rdi

let imm_int_on_stack i =
    (** allocates an integer i on the heap. Returns is address in rax *)
    allocate 2 ++
    movq (imm 1) (ind rax) ++
    movq (imm i) (ind ~ofs:8 rax)

let reg_int_on_stack reg =
    (** puts the integer in %reg on the heap and returns the address in rbx if reg=rax, else rax *)
    let tmp_reg = if reg = rax then r12 else rax in
    pushq !%tmp_reg ++
    (if reg = rax then movq !%rax !%tmp_reg else nop) ++
    allocate 2 ++
    movq (imm 1) (ind rax) ++
    movq !%tmp_reg (ind ~ofs:8 rax) ++
    popq tmp_reg
        
    
let imm_bool_on_stack b =
    allocate 2 ++
    movq (imm 2) (ind rax) ++
    movq (imm (if b then 1 else 0)) (ind ~ofs:8 rax)

let reg_bool_on_stack reg =
    let tmp_reg = if reg = rax then r12 else rax in
    (if reg = rax then movq !%rax !%tmp_reg else nop) ++
    pushq !%tmp_reg ++
    allocate 2 ++
    movq (imm 2) (ind rax) ++
    movq !%tmp_reg (ind ~ofs:8 rax) ++
    popq tmp_reg

let imm_string_on_stack (str:string) =
    (* we guarantee that if the same string is defined twice, it is at the same address. We can do this because strings are always defined explicitely *)
    (* fist, we look if this string is already defined or not *)
    let id = 
        try Hashtbl.find strings str
        with Not_found -> (
            (* if not, we put the string in the data segment *)
            Hashtbl.add strings str !string_counter;
            incr string_counter;
            (!string_counter -1))
    in
    allocate 2 ++
    movq (imm 3) (ind rax) ++
    movq (ilab ("_str_"^(string_of_int id))) (ind ~ofs:8 rax)




let type_to_pref t =
    match t with
    | Tnothing -> "_0"
    | Tany -> "_A"
    | Tint -> "_1"
    | Tbool -> "_2"
    | Tstring -> "_3"
    | Tstruct(id,_) -> "_"^(string_of_int (IdMap.find id !structs))
    | _ -> assert false



let string_to_type s =
    let char_to_type = function
    | '0' -> 0
    | '1' -> 1
    | '2' -> 2
    | '3' -> 3
    | '4' -> 4
    | '5' -> 5
    | '6' -> 6
    | '7' -> 7
    | '8' -> 8
    | '9' -> 9
    | _ -> assert false
    in
    let res = ref 0 in
    for i = String.length s - 1 downto 0 do
        res := 10* !res + char_to_type s.[i];
    done;
    !res
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
(* compilation *)

let rec compile_expr e =
match e with
    | Rcst _ -> compile_cst e
    | Rident _ -> compile_ident e 
    | Rbinop _ -> compile_binop e 
    | Runop _ -> compile_unop e
    | Rcall _ -> compile_call e (* TODO *)
    | Rif _ -> compile_if e
    | Rwhile _ -> compile_while e
    | Rfor _ -> compile_for e
    | Rseq _ -> compile_seq e
    | Rreturn _ -> compile_return e (* TODO *)
    | Rassign _ -> compile_assign e (* TODO : testing structs *)
    | Rlv _ -> compile_lv e (* TODO : testing structs *)

and compile_cst = function
    | Rcst(c,t,e) -> begin
        (* convert the value in the right memory representation *)
        let v = match c with
        | Cint i -> begin
            imm_int_on_stack i
        end
        | Cbool b -> imm_bool_on_stack b
        | Cnothing -> 
            compile_ident(Rident("nothing",Tany,e))
        | Cstring s -> 
            imm_string_on_stack s
        | Ccar car ->
            imm_string_on_stack (String.make 1 car)
        in
        v
    end
    | _ -> assert false
    
and compile_ident = function
    (* puts on top of the stack the pointer towards the value *)
    | Rident(id,t,_) -> begin
        (* a value that is either local or global *)
        (* we first check if it is a defined local variable *)
        try let ofs = (IdMap.find id !loc_var) in 
            movq (ind ~ofs:(8*ofs) rsp) !%rax
        with Not_found -> begin
            (* if not, we check if it is global *)
            try let _ = Hashtbl.find genv id in
                movq (indlab ("_var_"^id)) !%rax
            with Not_found -> failwith ("unbound variable : "^id)
        end
    end
    | _ -> assert false

and compile_bool_bin = function
    | Rbinop(Bor, r1, r2, t, _) -> begin
    let n = incr or_counter; string_of_int (!or_counter - 1) in
    compile_expr r1 ++
    movq !%rax !%rdi ++
    label ("__or_"^n) ++
    cmpq (imm 0) !%rdi ++
    je "__fail" ++
    cmpq (imm 2) (ind rdi) ++
    jne "__fail" ++
    cmpq (imm 1) (ind ~ofs:8 rdi) ++
    je ("__or_true_"^n) ++
    compile_expr r2 ++
    movq !%rax !%rdi ++
    cmpq (imm 0) !%rdi ++
    je "__fail" ++
    cmpq (imm 2) (ind rdi) ++
    jne "__fail" ++
    cmpq (imm 1) (ind ~ofs:8 rdi) ++
    je ("__or_true_"^n) ++
    imm_bool_on_stack false ++
    jmp ("__or_end_"^n) ++
    label ("__or_true_"^n) ++
    imm_bool_on_stack true ++
    label ("__or_end_"^n)
    end
    | Rbinop(Band, r1, r2, t, _) -> begin
    let n = incr and_counter; string_of_int (!and_counter - 1) in
    compile_expr r1 ++
    movq !%rax !%rdi ++
    label ("__and_"^n) ++
    cmpq (imm 0) !%rdi ++
    je "__fail" ++
    cmpq (imm 2) (ind rdi) ++
    jne "__fail" ++
    cmpq (imm 0) (ind ~ofs:8 rdi) ++
    je ("__and_false_"^n) ++
    compile_expr r2 ++
    movq !%rax !%rdi ++
    cmpq (imm 0) !%rdi ++
    je "__fail" ++
    cmpq (imm 2) (ind rdi) ++
    jne "__fail" ++
    cmpq (imm 0) (ind ~ofs:8 rdi) ++
    je ("__and_false_"^n) ++
    imm_bool_on_stack true ++
    jmp ("__and_end_"^n) ++
    label ("__and_false_"^n) ++
    imm_bool_on_stack false ++
    label ("__and_end_"^n)
    end
    | _ -> assert false

and compile_not_bool_bin = function
    | Rbinop(binop, r1, r2, t,_) ->
        compile_expr r1 ++
        pushq !%rax ++
        compile_expr r2 ++
        popq rdi ++
        movq !%rax !%rsi ++
        (match binop with
        | Badd -> call "__add" ++ reg_int_on_stack rax
        | Bsub -> call "__sub" ++ reg_int_on_stack rax
        | Bmul -> call "__mult" ++ reg_int_on_stack rax
        | Bpow -> call "__pow" ++ reg_int_on_stack rax
        | Bmod -> call "__modulo" ++ reg_int_on_stack rax
        | Band -> assert false
        | Bor -> assert false
        | Blt -> call "__less" ++ reg_bool_on_stack rax
        | Ble -> call "__less_or_equal"++ reg_bool_on_stack rax
        | Bgt -> call "__less_or_equal" ++ xorq (imm 1) !%rax ++ reg_bool_on_stack rax
        | Bge -> call "__less" ++ xorq (imm 1) !%rax ++ reg_bool_on_stack rax
        | Beq -> call "__equal" ++ reg_bool_on_stack rax
        | Bneq -> call "__equal" ++ xorq (imm 1) !%rax ++ reg_bool_on_stack rax)
    | _ -> assert false

and compile_binop r = match r with 
    | Rbinop(binop, r1, r2, t,_) ->
        (match binop with
        | Band | Bor -> compile_bool_bin r
        | _ -> compile_not_bool_bin r)
    | _ -> assert false
    
and compile_unop = function
    | Runop(unop, r, t,_) -> begin
        compile_expr r ++
        movq !%rax !%rdi ++
        (match unop with
         | Uneg -> 
            call "__neg" ++
            reg_int_on_stack rax
         | Unot -> 
            call "__not" ++
            reg_bool_on_stack rax)
    end
    | _ -> assert false
    
and compile_call = function
    | Rcall(id, args, t, e, s) -> 
        begin
        match id with
        | "print" -> begin 
        match args with
        | [] -> nop
        | r::tl -> 
        compile_expr r ++
            movq !%rax !%rsi ++
            call "__print" ++
            compile_call(Rcall("print", tl, t, e, s))
        end
        | "println" -> compile_call (Rcall("print", args, t, e, s)) ++
            compile_call (Rcall("print", [Rcst((Cstring("\n")), Tstring, e)], Tany, e, s))
        | _ -> (
        let n = List.length args in
        List.fold_left (fun prev r -> prev ++ (compile_expr r) ++ pushq !%rax) nop args ++ call ("_F_"^(string_of_int n)^"_"^id) ++
        addq (imm (8*n)) !%rsp)
        
        
        end
    | _ -> assert false

and compile_if = function
    | Rif(r1, r2, r3, t, _) -> begin
        let snum = incr ite_counter; string_of_int(!ite_counter - 1) in
        (* testing the condition *)
        compile_expr r1 ++
        cmpq (imm 0) !%rax ++
        je "__fail" ++
        cmpq (imm 2) (ind rax) ++
        jne "__fail" ++
        testq (imm 1) (ind ~ofs:8 rax) ++
        je ("_else_"^snum) ++
        compile_expr r2 ++
        jmp ("_endite_"^snum) ++
        label ("_else_"^snum) ++
        compile_expr r3 ++
        label ("_endite_"^snum)
    end
    | _ -> assert false

and compile_while = function
    | Rwhile(r1, r2, t, _, s) ->
        (* id of the loop *)
        let snum = incr while_counter; string_of_int (!while_counter-1) in
        (* number of variables intern, if changing scope *)
        let num_var = max_local_vars r2 in
        label ("_wb_"^snum) ++
        compile_expr r1 ++
        cmpq (imm 0) !%rax ++
        je "__fail" ++
        cmpq (imm 2) (ind rax) ++
        jne "__fail" ++
        testq (imm 1) (ind ~ofs:8 rax) ++
        je ("_we_"^snum) ++
        (if s = Global then
            (next := 0;
            subq (imm (8*num_var)) !%rsp) ++
            movq !%rsp !%r15
        else
            nop) ++
        compile_expr r2 ++
        (if s = Global then begin
            (* we free the local variables *)
            let l = ref [] in
            for i=0 to num_var - 1 do
                l := (  movq (ind ~ofs:(8*i) r15) !%rdi ++
                        cmpq (imm 0) !%rdi ++
                        je ("_wf_"^snum^(string_of_int i)) ++
                        call "free" ++
                        label ("_wf_"^snum^(string_of_int i))
                    ) :: !l
            done;
            next := 0;
            (List.fold_left (++) nop !l) ++
            addq (imm num_var) !%r15 ++
            movq !%r15 !%rsp
            end
        else
            nop) ++
        jmp ("_wb_"^snum) ++
        label ("_we_"^snum) ++
        movq (imm 0) !%rax

    | _ -> assert false

and compile_for = function
    (* TODO : test memory freeing *)
    | Rfor(id, r1, r2, r3, t, e, s) -> begin
        let snum = incr for_counter; string_of_int(!for_counter - 1) in
        let loc_var_backup = !loc_var in
        next := 0;
        loc_var := IdMap.add ("_idx_"^snum) !next !loc_var;
        let ofs_idx = !next in
        incr next;
        loc_var := IdMap.add ("_bound_"^snum) !next !loc_var;
        let ofs_bound = !next in
        incr next;
        next := 0;
        let res1 =
            (if s = Global then
                subq (imm 16) !%rsp else nop) ++
            (* compute the initial value *)
            compile_expr r1 ++
            (* check it is integer ... *)
            cmpq (imm 0) !%rax ++
            je "__fail" ++
            cmpq (imm 1) (ind rax) ++
            jne "__fail" ++
            (* initialize the index *)
            movq !%rax (ind ~ofs:(8*ofs_idx) rsp) ++
            (* compute the final value *)
            compile_expr r2 ++
            cmpq (imm 0) !%rax ++
            je "__fail" ++
            cmpq (imm 1) (ind rax) ++
            jne "__fail" ++
            (* initialize the upper bound *)
            movq !%rax (ind ~ofs:(8*ofs_bound) rsp)
        in
        let num_var = if s=Global then max_local_vars r3 + 1 else 1 in
        (** now we can enter the loop *)
            (* initialize the variable id *)
        let ofs_var_idx = incr next; !next -1 in
        loc_var := IdMap.add id ofs_var_idx (!loc_var);
        let res2=
            label ("_fb_"^snum)++
            (* make another turn of the loop ?*)
            movq (ind ~ofs:(8*ofs_idx) rsp) !%rax ++ (* address of the counter *)
            movq (ind ~ofs:8 rax) !%rax ++ (* value of the counter *)
            movq (ind ~ofs:(8*ofs_bound) rsp) !%rcx ++
            cmpq (ind ~ofs:8 rcx) !%rax ++
            jg ("_fe_"^snum) ++
            
            subq (imm (8*num_var)) !%rsp
        in
        loc_var := IdMap.add ("_idx_"^snum) (ofs_idx+num_var) !loc_var;
        loc_var := IdMap.add ("_bound_"^snum) (ofs_bound+num_var) !loc_var;
        let res3 =
            (* reset the counter *)
            movq (ind ~ofs:(8*(ofs_idx+num_var)) rsp) !%rax ++ 
            movq !%rax (ind ~ofs:(8*(ofs_var_idx)) rsp)++
            
            
            (* body of the loop *)
            compile_expr r3 ++           
            (* increment the counter *)
            addq (imm (8*num_var)) !%rsp ++
            movq (ind ~ofs:(8*(ofs_idx)) rsp) !%rax ++
            movq (ind ~ofs:8 rax) !%rcx ++
            addq (imm 1) !%rcx ++
            movq !%rcx (ind ~ofs:8 rax) ++
            movq !%rax (ind ~ofs:(8*(ofs_idx)) rsp) ++
            jmp ("_fb_"^snum) ++
            
            label ("_fe_"^snum) ++
            (if s = Global then addq (imm 16) !%rsp else nop) ++
            movq (ilab "_var_nothing") !%rax
        in
        loc_var := loc_var_backup;
        res1 ++ res2 ++ res3
    end
    | _ -> assert false

and compile_seq = function
    | Rseq ([], t, _) -> nop
    | Rseq( h::tl, t, e) -> begin 
    let x_hd = compile_expr h in
    let x_tl = compile_seq (Rseq(tl,t,e)) in
    x_hd ++ x_tl
    end
    | _ -> assert false

and compile_return = function
    | Rreturn(r, t, _) -> 
        compile_expr r ++
        ret
    | _ -> assert false

and compile_assign = function
    | Rassign(lv, r, t, e) -> begin
        match lv with
        | Rlvid id -> begin
            (* we first determine if it is a global or a local variable *)
            if snd (IdentEnv.find id e) = Local then
            begin (* it is local *)
                (* is it already defined ? *)
                if IdMap.mem id !loc_var then
                    (* get the binding and change it *)
                    let ofs = 8*(IdMap.find id !loc_var) in
                    compile_expr r ++
                    (* the memory is already allocated, we just need to move pointers around *)
                    movq !%rax (ind ~ofs rsp)
                else
                    (* it is not already defined *)
                    (* we find the smallest non occupied memory slot *)
                    let offset = incr next; !next -1 in
                    loc_var := IdMap.add id offset (!loc_var);
                    (* now, we can properly assign the value in memory *)
                    compile_expr r ++
                    movq !%rax (ind ~ofs:(8*offset) rsp)
            end
            else (* it is global *)
                (* it is defined in the global data segment, at the label 'id' *)
                (* we need to check if it is its first assignment *)
                (if not (Hashtbl.mem genv id) then Hashtbl.add genv id ();
                compile_expr r) ++
                movq !%rax (indlab ("_var_"^id))
        end
        | Rlvfield(r2, id) -> begin
            (* easier: it must already be a structure, initialization via function call *)
            let struct_name = IdMap.find id !name_of_struct in
            let type_num = IdMap.find struct_name !structs in
            let field = IdMap.find id !num_of_field in
            compile_expr r2 ++
            cmpq (imm 0) !%rax ++
            je "__fail" ++
            cmpq (imm type_num) (ind rax) ++
            jne "__fail" ++
            pushq !%rax ++
            compile_expr r ++
            movq !%rax !%r12 ++
            popq rax ++
            movq !%r12 (ind ~ofs:(8*(field+1)) rax)
        end
    end
    | _ -> assert false

and compile_lv = function
    | Rlv(Rlvid(id), t, e) -> compile_ident(Rident(id,t,e))
    | Rlv(Rlvfield(r,id), t, e) -> 
    (* get the type of the structure and its number, based on the name of the field *)
    let struct_name = IdMap.find id !name_of_struct in
    let type_num = IdMap.find struct_name !structs in
    let field = IdMap.find id !num_of_field in
    compile_expr r ++
    cmpq (imm 0) !%rax ++
    je "__fail" ++
    cmpq (imm type_num) (ind rax) ++
    jne "__fail" ++
    movq (ind ~ofs:(8*(field+1)) rax) !%rax
    | _ -> assert false

and compile_struct = function
    | RDstruct(_,id,params) -> begin
        let struct_num = !next_struct in
        incr next_struct;
        structs:= IdMap.add id struct_num !structs; 
        let field_count = List.length params in
        (* get list of name of fields *)
        let get_field_name p =
        match fst p with
        | Ast.Eident id -> id
        | _ -> assert false
        in
        let names = List.map get_field_name params in
        name_of_struct := List.fold_left (fun prev n -> IdMap.add n id prev) !name_of_struct names;
        let ids = List.mapi (fun i _ -> i) names in
        num_of_field := List.fold_left2 (fun prev n id -> IdMap.add n id prev) !num_of_field names ids;
        (* define the constructor now *)
        let loc_var_backup = !loc_var in
        let param_to_pref p = match snd p with
        | None | Some("Any") -> "_A"
        | Some("Nothing") -> "_0"
        | Some("Int64") -> "_1"
        | Some("Bool") -> "_2"
        | Some("String") -> "_3"
        | Some(s) -> "_"^string_of_int(IdMap.find s !structs)
        in
        let prefix = 
            (List.fold_left (fun prev p -> prev^(param_to_pref p)) "_f" params)^"_r_"^(string_of_int struct_num)^"_"
        in
        let () = (List.iteri(fun i p -> loc_var := IdMap.add (nameofparam p) (field_count+i+1) !loc_var) params);
        next := 0;
        Hashtbl.add func_sig id prefix in
        let res =
            label (prefix^id) ++
            (* save return address *)
            movq !%rsp !%r15 ++
            (* save the retrun block address *)
            pushq (ilab (prefix^id^"_ret")) ++
            (* parameter on the stack : rbp - 8*(number_of_the_argument +1) *)
            let def = 
            List.fold_left (fun prev p ->
                            let i = IdMap.find (nameofparam p) !loc_var in
                            prev ++
                            movq (ind ~ofs:(8*(field_count+1-(i-field_count))) r15) !%rcx ++
                            movq !%rcx (ind ~ofs:(8*(i-field_count)) rax)
                            ) (movq (imm (struct_num)) (ind rax)) params
            in
            (* allocate the structure on the heap *)
            pushq !%rdi ++
            movq (imm (8*(field_count+1))) !%rdi ++
            call "malloc" ++
            popq rdi ++
            def ++
            label (prefix^id^"_ret") ++
            ret
        in
        loc_var := loc_var_backup; 
        res
    end
    | _ -> assert false

and compile_decl d = match d with
    | RDexpr(e) -> expr := !expr ++ (compile_expr e)
    | RDstruct _ -> decl := !decl ++ (compile_struct d)
    | RDfunction _ -> decl := !decl ++ (compile_function d)

and compile_decl_d d = match d with
    | RDexpr(e) -> nop
    | RDstruct _ -> compile_struct d (* TODO : constructor *)
    | RDfunction _ -> compile_function d (* TODO : test *)

and compile_decl_e = function
    | RDexpr(e) -> compile_expr e
    | _ -> nop


and compile_function = function
    | RDfunction(id, params, t, body) -> begin
        let loc_var_backup = !loc_var in
        let param_to_pref p = match snd p with
        | None | Some("Any") -> "_A"
        | Some("Nothing") -> "_0"
        | Some("Int64") -> "_1"
        | Some("Bool") -> "_2"
        | Some("String") -> "_3"
        | Some(s) -> "_"^string_of_int(IdMap.find s !structs)
        in
        let type_to_pref = function
        | Tany -> "_A"
        | Tnothing -> "_0"
        | Tint -> "_1"
        | Tbool -> "_2"
        | Tstring -> "_3"
        | Tstruct(id,_) -> "_"^string_of_int(IdMap.find id !structs)
        | _ -> assert false
        in
        let prefix = 
            (List.fold_left (fun prev p -> prev^(param_to_pref p)) "_f" params)^"_r"^(type_to_pref t)^"_"
        in
        Hashtbl.add func_sig id prefix;
        let n = max_local_vars body in
        let res =
            label (prefix^id) ++
            (* save the retrun block address *)
            pushq (ilab (prefix^id^"_ret")) ++
            (* save return address *)
            movq !%rsp !%r15 ++
            (* make room for local variables *)
            subq (imm (8*n)) !%rsp ++
            (* they are undefined *)
            (* (let res = ref nop in
            for i=0 to n do
                res := !res ++ movq (imm 0) (ind ~ofs:(8*i) rsp)
            done;
            !res) ++ *)
            
            
            (* parameter on the stack : rbp - 8*(number_of_the_argument +1) *)
            (List.iteri(fun i p -> loc_var := IdMap.add (nameofparam p) (n+i+1) !loc_var) params;
            next := 0;
            compile_expr body) ++
            addq (imm (8*n)) !%rsp ++
            label (prefix^id^"_ret") ++
            (* check type of return *)
            (match t with
            | Tany -> ret
            | Tnothing ->
                cmpq (imm 0) !%rax ++
                jne "__fail" ++
                ret
            | Tint ->
                cmpq (imm 0) !%rax ++
                je "__fail" ++
                cmpq (imm 1) (ind rax) ++
                jne "__fail" ++
                ret
            | Tbool ->
                cmpq (imm 0) !%rax ++
                je "__fail" ++
                cmpq (imm 2) (ind rax) ++
                jne "__fail" ++
                ret
            | Tstring ->
                cmpq (imm 0) !%rax ++
                je "__fail" ++
                cmpq (imm 3) (ind rax) ++
                jne "__fail" ++
                ret
            | Tstruct(id,_) -> let num = IdMap.find id !structs in
                cmpq (imm 0) !%rax ++
                je "__fail" ++
                cmpq (imm num) (ind rax) ++
                jne "__fail" ++
                ret    
            | Tfunc _ -> assert false)
        in
        loc_var := loc_var_backup;
        res
    end
    | _ -> assert false



let division_call =
    begin
        let id = "div" in
        let prefix = "_f_1_1_r_1_" in
        Hashtbl.add func_sig id prefix;
        let res = label (prefix^id) ++
        (* save return address *)
        movq !%rsp !%r15 ++
        (* save the retrun block address *)
        pushq (ilab (prefix^id^"_ret")) ++
        (* parameter on the stack : rbp - 8*(number_of_the_argument +1) *)
        
        movq (ind ~ofs:(-8) rbp) !%rdi ++
        movq (ind ~ofs:(-16) rbp) !%rsi ++
        (next := 0;
        call "__division") ++
        ret ++
        label (prefix^id^"_ret") ++
        movq (imm 2) !%rdi ++
        pushq !%r13 ++
        movq !%rax !%r13 ++
        xorq !%rax !%rax ++
        call "malloc" ++
        movq (imm 1) (ind rax) ++
        movq !%r13 (ind ~ofs:8 rax) ++
        popq rdx ++
        ret in
        res
    end

let compile_dispatch id n =
    (* we get all the functions' signature that share the name id, with n arguments *)
        let prefixes = List.filter (fun p -> List.length (String.split_on_char '_' p)= n+5) (Hashtbl.find_all func_sig id) in
        (* partition the prefixes given the number of Any they got *)
        let prefix_any = Array.make (n+1) [] in
        let any_cnt pref =
            let res = ref 0 in
            let i = ref 0 in
            while !i< String.length pref -1 && pref.[!i] != 'r' do
                if pref.[!i] = 'A' then
                    incr res;
                incr i;
            done;
            !res
        in
        (List.iter (fun p -> prefix_any.(any_cnt p) <- p::prefix_any.(any_cnt p) ) prefixes);
        (* for each partition, we make the decision list, by remembering if we have already found a suitable function *)
        let decision p = (* for one prefix *)
            let res = ref nop in
            let types = String.split_on_char '_' p in
            (List.iteri (fun i t ->
                            if t = "A" || i > n+1|| i <= 1 then ()
                            else begin
                                res := !res ++ movq (ind ~ofs:(8*(n-i+1)) rsp) !%rax ++
                                movq (ind rax) !%rax ++
                                cmpq (imm (string_to_type t)) !%rax ++
                                jne (p^"t_"^id) ++
                                cmpq (imm 0) !%r8 ++
                                jne "__fail" ++
                                movq (ilab (p^id)) !%r8
                            end
                         )
                         ) types;
            label (p^"s_"^id) ++
            (if !res <> nop then !res else movq (ilab (p^id)) !%r8) ++
            label (p^"t_"^id)
        in
        incr skip_counter;
        Array.fold_left (fun prev l -> prev ++ List.fold_left (fun prev pref -> prev ++ decision pref) nop l) (label ("_F_"^(string_of_int n)^"_"^id) ++ (xorq !%r8 !%r8)) prefix_any ++
        cmpq (imm 0) !%r8 ++
        je ("_skip_"^(string_of_int (!skip_counter - 1))) ++
        (jmp_star !%r8) ++
        label ("_skip_"^(string_of_int (!skip_counter - 1)))
        
        
        

let compile prog =
    let header = (globl "main") ++ addition ++ substraction ++ multiplication ++ exponentiation ++ modulo ++ division ++ less ++ less_or_equal ++ equal ++ negation ++ not_unop ++ print in
    let footer = end_main ++ fail in
    List.iter compile_decl (fst prog);
    let text_decl = !decl in
    let function_decl =
        let idsns = ref [] in
        Hashtbl.iter (fun k v ->
                      idsns := (k, List.length (String.split_on_char '_' v) - 5) :: !idsns;
                      ) func_sig;
        List.fold_left (fun prev (id,n) -> prev ++ compile_dispatch id n) nop (List. sort_uniq compare !idsns) ++
        cmpq (imm 0) !%r8 ++
        je "__fail"
        
    in
    let text_expr = !expr in
    let data = Hashtbl.fold (fun k v prev -> prev ++ label ("_var_"^k) ++ dquad [0]) genv nop ++
    Hashtbl.fold (fun k v prev -> prev ++ label k ++ dquad [0]) idxenv nop in
    let messages = 
        label "__msg_nothing" ++
        string "nothing" ++
        label "__msg_int" ++
        string "%d" ++
        label "__msg_true" ++
        string "true" ++
        label "__msg_false" ++
        string "false" ++
        label "__msg_error" ++
        string "Error!"
    in
    let asm_strings = Hashtbl.fold (fun k v prev -> prev ++ label ("_str_"^(string_of_int v)) ++ string k) strings nop in
    {text=header++text_decl++function_decl++division_call++main++text_expr++footer ; data=data++messages++asm_strings}
